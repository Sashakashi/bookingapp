package by.itstep.bookingapp.controller;
import by.itstep.bookingapp.dto.user.*;
import by.itstep.bookingapp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/users/{id}")
    public UserFullDto findById(@PathVariable Integer id){
        return userService.findById(id);
    }

    @GetMapping("/users")
    public List<UserShortDto> findAll(){
        return userService.findAll();
    }

    @PostMapping("/users")
    public UserFullDto create(@Valid @RequestBody UserCreateDto dto){
        return userService.create(dto);
    }

    @PutMapping("/users")
    public UserFullDto update(@Valid @RequestBody UserUpdateDto dto) {
        return userService.update(dto);
    }

    @PutMapping("/users/password")
    public void changePassword(@Valid @RequestBody ChangeUserPasswordDto dto){
        userService.changePassword(dto);
    }

    @PutMapping("/users/role")
    public UserFullDto changeRole(@Valid @RequestBody ChangeUserRoleDto dto){
        return userService.changeRole(dto);
    }

    @DeleteMapping("/users/{id}")
    public void delete (@PathVariable Integer id){
        userService.delete(id);
    }

    @PutMapping("/users/boost")
    public void boostBalance (@Valid @RequestBody BoostBalanceDto dto){
        userService.boostBalance(dto);
    }

}