package by.itstep.bookingapp.controller;
import by.itstep.bookingapp.dto.booking.BookingCreateDto;
import by.itstep.bookingapp.dto.booking.BookingFullDto;
import by.itstep.bookingapp.service.BookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@RestController
public class BookingController {

    @Autowired
    private BookingService bookingService;

    @GetMapping("/bookings/{id}")
    public BookingFullDto findById(@PathVariable Integer id){
        return bookingService.findById(id);
    }

    @GetMapping("/bookings")
    public List<BookingFullDto> findAll(){
        return bookingService.findAll();
    }

    @PostMapping("/bookings")
    public BookingFullDto createAndPay(@Valid @RequestBody BookingCreateDto dto){
        return bookingService.createAndPay(dto);
    }

    @DeleteMapping("/bookings/{id}")
    public void deleteAndRefund(@PathVariable Integer id){
        bookingService.deleteAndRefund(id);
    }



}