package by.itstep.bookingapp.controller;
import by.itstep.bookingapp.dto.country.CountryCreateDto;
import by.itstep.bookingapp.dto.country.CountryFullDto;
import by.itstep.bookingapp.service.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@RestController
public class CountryController {

    @Autowired
    private CountryService countryService;

    @GetMapping("/countries/{id}")
    public CountryFullDto findById(Integer id){
        return countryService.findById(id);
    }

    @GetMapping("/countries")
    public List<CountryFullDto> findAll(){
        return countryService.findAll();
    }

    @PostMapping("/countries")
    public CountryFullDto create(@Valid @RequestBody CountryCreateDto dto){
        return countryService.create(dto);
    }

    @DeleteMapping("/countries/{id}")
    public void delete (@PathVariable Integer id){
        countryService.delete(id);
    }

}