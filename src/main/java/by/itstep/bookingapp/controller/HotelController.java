package by.itstep.bookingapp.controller;
import by.itstep.bookingapp.dto.hotel.HotelCreateDto;
import by.itstep.bookingapp.dto.hotel.HotelFullDto;
import by.itstep.bookingapp.dto.hotel.HotelShortDto;
import by.itstep.bookingapp.dto.hotel.HotelUpdateDto;
import by.itstep.bookingapp.dto.room.RoomShortDto;
import by.itstep.bookingapp.service.HotelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@RestController
public class HotelController {

    @Autowired
    private HotelService hotelService;

    @GetMapping("/hotels/{id}")
    public HotelFullDto findById(@PathVariable Integer id){
        return hotelService.findById(id);
    }

    @GetMapping("/hotels")
    public List<HotelShortDto> findAll(){
        return hotelService.findAll();
    }

    @PostMapping("/hotels")
    public HotelFullDto create(@Valid @RequestBody HotelCreateDto dto){
        return hotelService.create(dto);
    }

    @PutMapping("/hotels")
    public HotelFullDto update(@Valid @RequestBody HotelUpdateDto dto){
        return hotelService.update(dto);
    }

    @DeleteMapping("/hotels/{id}")
    public void delete(@PathVariable Integer id){
        hotelService.delete(id);
    }

    @GetMapping("/hotels/{id}/rooms")
    public List<RoomShortDto> findAllRooms(@PathVariable Integer id){
        return hotelService.findAllRoomsInHotel(id);
    }

    @DeleteMapping("/hotels/{id}/delete/{numberOfPeople}")
    public HotelFullDto deleteRoom(@PathVariable Integer id, @PathVariable Integer numberOfPeople){
        return hotelService.deleteRoom(id, numberOfPeople);
    }

    @DeleteMapping("/hotels/{id}/add/{numberOfPeople}")
    public HotelFullDto addRoom(@PathVariable Integer id, @PathVariable Integer numberOfPeople){
        return hotelService.addRoom(id, numberOfPeople);
    }


}