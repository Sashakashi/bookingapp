package by.itstep.bookingapp.mapper;
import by.itstep.bookingapp.dto.booking.BookingCreateDto;
import by.itstep.bookingapp.dto.booking.BookingFullDto;
import by.itstep.bookingapp.entities.BookingEntity;
import by.itstep.bookingapp.repository.UserRepository;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Mapper(componentModel = "spring")
public interface BookingMapper {

    @Mapping(target = "userId", expression = "java(entity.getUser().getId())")
    BookingFullDto map (BookingEntity entity);

    BookingEntity map (BookingCreateDto dto);

    List<BookingFullDto> map (List <BookingEntity> entities);


}
