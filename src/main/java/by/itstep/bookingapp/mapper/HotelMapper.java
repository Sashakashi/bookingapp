package by.itstep.bookingapp.mapper;
import by.itstep.bookingapp.dto.hotel.HotelCreateDto;
import by.itstep.bookingapp.dto.hotel.HotelFullDto;
import by.itstep.bookingapp.dto.hotel.HotelShortDto;
import by.itstep.bookingapp.entities.HotelEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import java.util.List;

@Mapper(componentModel = "spring", uses = RoomMapper.class)
public interface HotelMapper {

    @Mapping(target = "countryId", expression = "java(entity.getCountry().getId())")
    HotelFullDto map (HotelEntity entity);

    HotelEntity map (HotelCreateDto dto);

    @Mapping(target = "countryId", expression = "java(entity.getCountry().getId())")
    List<HotelShortDto> map (List <HotelEntity> entities);

}
