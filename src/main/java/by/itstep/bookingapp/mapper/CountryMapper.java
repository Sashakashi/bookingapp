package by.itstep.bookingapp.mapper;
import by.itstep.bookingapp.dto.country.CountryCreateDto;
import by.itstep.bookingapp.dto.country.CountryFullDto;
import by.itstep.bookingapp.entities.CountryEntity;
import org.mapstruct.Mapper;
import java.util.List;

@Mapper(componentModel = "spring", uses = HotelMapper.class)
public interface CountryMapper {

    CountryFullDto map (CountryEntity entity);

    CountryEntity map (CountryCreateDto dto);

    List<CountryFullDto> map (List <CountryEntity> entities);

}
