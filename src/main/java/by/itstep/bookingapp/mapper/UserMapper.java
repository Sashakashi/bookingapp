package by.itstep.bookingapp.mapper;
import by.itstep.bookingapp.dto.user.UserCreateDto;
import by.itstep.bookingapp.dto.user.UserFullDto;
import by.itstep.bookingapp.dto.user.UserShortDto;
import by.itstep.bookingapp.entities.UserEntity;
import org.mapstruct.Mapper;
import java.util.List;

@Mapper(componentModel = "spring", uses = BookingMapper.class)
public interface UserMapper {

    UserFullDto map (UserEntity entity);

    UserEntity map (UserCreateDto dto);

    List<UserShortDto> map (List <UserEntity> entities);


}
