package by.itstep.bookingapp.mapper;
import by.itstep.bookingapp.dto.room.RoomCreateDto;
import by.itstep.bookingapp.dto.room.RoomFullDto;
import by.itstep.bookingapp.dto.room.RoomShortDto;
import by.itstep.bookingapp.entities.RoomEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import java.util.List;

@Mapper(componentModel = "spring", uses = BookingMapper.class)
public interface RoomMapper {

    RoomFullDto map (RoomEntity entity);

    RoomEntity map (RoomCreateDto dto);

    List<RoomShortDto> map (List <RoomEntity> entities);

}
