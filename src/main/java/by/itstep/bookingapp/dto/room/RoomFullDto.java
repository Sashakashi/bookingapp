package by.itstep.bookingapp.dto.room;
import by.itstep.bookingapp.dto.booking.BookingFullDto;
import lombok.Data;
import java.util.ArrayList;
import java.util.List;

@Data
public class RoomFullDto {

    private Integer id;

    private Integer numberOfPeople;

    private Double priceForRoom;

    private Integer hotelId;

    private List<BookingFullDto> bookings = new ArrayList<>();

}
