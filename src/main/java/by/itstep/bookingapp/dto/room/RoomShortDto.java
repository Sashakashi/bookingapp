package by.itstep.bookingapp.dto.room;
import lombok.Data;

@Data
public class RoomShortDto {

    private Integer id;

    private Integer numberOfPeople;

    private Double priceForRoom;

    private Integer hotelId;
}
