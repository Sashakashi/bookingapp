package by.itstep.bookingapp.dto.room;
import lombok.Data;
import javax.validation.constraints.NotNull;

@Data
public class RoomCreateDto {

    @NotNull
    private Integer numberOfPeople;

    @NotNull
    private Double priceForRoom;

    @NotNull
    private Integer hotelId;

}
