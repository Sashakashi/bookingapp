package by.itstep.bookingapp.dto.room;
import lombok.Data;
import javax.validation.constraints.NotNull;

@Data
public class RoomUpdateDto {

    @NotNull
    private Integer id;

    @NotNull
    private Integer numberOfPeople;

    @NotNull
    private Double priceForRoom;

}
