package by.itstep.bookingapp.dto.user;
import by.itstep.bookingapp.entities.UserRole;
import lombok.Data;
import javax.validation.constraints.NotNull;

@Data
public class ChangeUserRoleDto {

    @NotNull
    private Integer id;

    @NotNull
    private UserRole newRole;
}
