package by.itstep.bookingapp.dto.user;
import lombok.Data;

@Data
public class UserShortDto {

    private Integer id;

    private String name;

    private String lastname;

    private String phone;

}
