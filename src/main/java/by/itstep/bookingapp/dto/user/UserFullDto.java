package by.itstep.bookingapp.dto.user;
import by.itstep.bookingapp.dto.booking.BookingFullDto;
import by.itstep.bookingapp.entities.UserRole;
import lombok.Data;
import java.util.ArrayList;
import java.util.List;

@Data
public class UserFullDto {

    private Integer id;

    private String name;

    private String lastname;

    private String phone;

    private String email;

    private UserRole role;

    private Double balance;

    private List<BookingFullDto> bookings = new ArrayList<>();
}
