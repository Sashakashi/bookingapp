package by.itstep.bookingapp.dto.user;
import lombok.Data;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class BoostBalanceDto {

    @NotNull
    private Integer id;

    @NotBlank
    @Size(min = 8)
    private String Password;

    @NotBlank
    @Email
    private String email;

    @NotNull
    private Double newBalance;
}
