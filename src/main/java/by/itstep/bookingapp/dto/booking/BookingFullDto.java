package by.itstep.bookingapp.dto.booking;
import lombok.Data;
import java.sql.Date;

@Data
public class BookingFullDto {

    private Integer id;

    private Integer numberOfPeople;

    private Date dateIn;

    private Date dateOut;

    private Integer roomId;

    private Integer userId;
}
