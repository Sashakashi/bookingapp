package by.itstep.bookingapp.dto.booking;
import lombok.Data;
import javax.validation.constraints.NotNull;
import java.sql.Date;

@Data
public class BookingCreateDto {

    @NotNull
    private Integer numberOfPeople;

    private Date dateIn;

    private Date dateOut;

    @NotNull
    private Integer hotelId;

    @NotNull
    private Integer userId;
}
