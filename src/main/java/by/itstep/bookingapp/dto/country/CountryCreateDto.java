package by.itstep.bookingapp.dto.country;
import lombok.Data;

@Data
public class CountryCreateDto {

    private String name;

}
