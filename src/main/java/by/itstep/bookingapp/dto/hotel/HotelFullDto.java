package by.itstep.bookingapp.dto.hotel;
import by.itstep.bookingapp.dto.room.RoomShortDto;
import lombok.Data;
import java.util.ArrayList;
import java.util.List;

@Data
public class HotelFullDto {

    private Integer id;

    private String name;

    private String address;

    private Double price;

    private Integer numberOfRoomsFor1;

    private Integer numberOfRoomsFor2;

    private Integer numberOfRoomsFor3;

    private Integer numberOfRoomsFor4;

    private Integer stars;

    private Integer rating;

    private Integer countryId;

    private List<RoomShortDto> rooms = new ArrayList<>();

}
