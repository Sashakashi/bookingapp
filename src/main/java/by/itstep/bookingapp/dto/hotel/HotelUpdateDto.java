package by.itstep.bookingapp.dto.hotel;
import lombok.Data;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class HotelUpdateDto {

    @NotNull
    private Integer id;

    @NotBlank
    private String name;

    @NotBlank
    private String address;

    @NotNull
    private Double price;

    @NotNull
    private Integer countryId;


}
