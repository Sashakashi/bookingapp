package by.itstep.bookingapp.dto.hotel;
import lombok.Data;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class HotelCreateDto {

    @NotBlank
    private String name;

    @NotBlank
    private String address;

    @NotNull
    private Double price;

    @NotNull
    private Integer countryId;

    private Integer numberOfRoomsFor1;

    private Integer numberOfRoomsFor2;

    private Integer numberOfRoomsFor3;

    private Integer numberOfRoomsFor4;

}
