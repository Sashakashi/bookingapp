package by.itstep.bookingapp.dto.hotel;
import lombok.Data;

@Data
public class HotelShortDto {

    private Integer id;

    private String name;

    private String address;

    private Double price;

    private Integer countryId;

}
