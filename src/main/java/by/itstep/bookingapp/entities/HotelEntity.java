package by.itstep.bookingapp.entities;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.Where;
import javax.persistence.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "hotels")
@Where(clause = "deleted_at IS NULL")
public class HotelEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "address")
    private String address;

    @Column(name = "stars")
    private Integer stars;

    @Column(name = "rating")
    private Integer rating;

    @Column(name = "price")
    private Double price;

    @Column(name = "number_of_rooms_for_1")
    private Integer numberOfRoomsFor1;

    @Column(name = "number_of_rooms_for_2")
    private Integer numberOfRoomsFor2;

    @Column(name = "number_of_rooms_for_3")
    private Integer numberOfRoomsFor3;

    @Column(name = "number_of_rooms_for_4")
    private Integer numberOfRoomsFor4;

    @Column(name = "deleted_at")
    private Instant deletedAt;

    @ManyToOne
    @JoinColumn(name = "country_id")
    private CountryEntity country;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "hotel", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<RoomEntity> rooms = new ArrayList<>();

}
