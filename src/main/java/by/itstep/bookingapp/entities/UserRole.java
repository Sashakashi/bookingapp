package by.itstep.bookingapp.entities;

public enum UserRole {

    USER, ADMIN

}
