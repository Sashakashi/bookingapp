package by.itstep.bookingapp.entities;
import lombok.Data;
import org.hibernate.annotations.Where;
import javax.persistence.*;
import java.sql.Date;
import java.time.Instant;

@Data
@Entity
@Table(name = "bookings")
@Where(clause = "deleted_at IS NULL")
public class BookingEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "number_of_people")
    private Integer numberOfPeople;

    @Column(name = "date_in")
    private Date dateIn;

    @Column(name = "date_out")
    private Date dateOut;

    @Column(name = "deleted_at")
    private Instant deletedAt;

    @ManyToOne
    @JoinColumn(name = "room_id")
    private RoomEntity room;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserEntity user;

}
