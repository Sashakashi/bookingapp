package by.itstep.bookingapp.security;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import static org.springframework.security.config.http.SessionCreationPolicy.STATELESS;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private static final String[] SWAGGER_RESOURCES = {"/v2/api-docs",
            "/configuration/ui", "/swagger-resources/**",
            "/configuration/security", "/swagger-ui.html", "/webjars/**"};

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        http
                .httpBasic().disable()
                .csrf().disable()
                .sessionManagement().sessionCreationPolicy(STATELESS)
                .and()
                .authorizeRequests()

                .antMatchers(SWAGGER_RESOURCES).permitAll()
                .antMatchers(HttpMethod.GET).permitAll()
                .antMatchers(HttpMethod.POST, "/users/**", "/authenticate/**").permitAll()

                .antMatchers(HttpMethod.POST,"/bookings/**").hasAnyRole("ADMIN", "USER")
                .antMatchers(HttpMethod.PUT,"/users", "/users/password", "/users/boost", "/bookings/**")
                            .hasAnyRole("ADMIN", "USER")
                .antMatchers(HttpMethod.PUT,"/users/role").hasRole("ADMIN")
                .antMatchers(HttpMethod.DELETE,"/users/**", "/bookings/**").hasAnyRole("ADMIN", "USER")

                .antMatchers(HttpMethod.POST, "/hotels/**", "/countries/**").hasRole("ADMIN")
                .antMatchers(HttpMethod.DELETE, "/hotels/**", "/countries/**").hasRole("ADMIN")
                .antMatchers(HttpMethod.PUT, "/hotels/**", "/countries/**").hasRole("ADMIN")

                .anyRequest().authenticated()
                .and()
                .apply(new JwtConfigurer(jwtTokenProvider));
    }

    @Bean
    public static PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}

