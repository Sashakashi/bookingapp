package by.itstep.bookingapp.service;
import by.itstep.bookingapp.dto.booking.BookingCreateDto;
import by.itstep.bookingapp.dto.booking.BookingFullDto;
import by.itstep.bookingapp.dto.country.CountryFullDto;

import java.util.List;

public interface BookingService {

    BookingFullDto createAndPay(BookingCreateDto dto);

    BookingFullDto findById(Integer id);

    List<BookingFullDto> findAll();

    void deleteAndRefund(Integer id);


}
