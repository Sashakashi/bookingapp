package by.itstep.bookingapp.service.impl;
import by.itstep.bookingapp.dto.hotel.HotelCreateDto;
import by.itstep.bookingapp.dto.hotel.HotelFullDto;
import by.itstep.bookingapp.dto.hotel.HotelShortDto;
import by.itstep.bookingapp.dto.hotel.HotelUpdateDto;
import by.itstep.bookingapp.dto.room.RoomShortDto;
import by.itstep.bookingapp.entities.CountryEntity;
import by.itstep.bookingapp.entities.HotelEntity;
import by.itstep.bookingapp.entities.RoomEntity;
import by.itstep.bookingapp.exception.IllegalAmountOfPeopleException;
import by.itstep.bookingapp.mapper.HotelMapper;
import by.itstep.bookingapp.mapper.RoomMapper;
import by.itstep.bookingapp.repository.CountryRepository;
import by.itstep.bookingapp.repository.HotelRepository;
import by.itstep.bookingapp.repository.RoomRepository;
import by.itstep.bookingapp.service.HotelService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityNotFoundException;
import java.time.Instant;
import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class HotelServiceImpl implements HotelService {

    private final HotelMapper hotelMapper;
    private final HotelRepository hotelRepository;
    private final RoomRepository roomRepository;
    private final CountryRepository countryRepository;
    private final RoomMapper roomMapper;

    @Override
    @Transactional
    public HotelFullDto create(HotelCreateDto createDto) {
        HotelEntity entityToCreate = hotelMapper.map(createDto);
        entityToCreate.setStars(0);
        entityToCreate.setRating(0);
        CountryEntity country = countryRepository.getById(createDto.getCountryId());
        entityToCreate.setCountry(country);

        HotelEntity createdEntity = hotelRepository.save(entityToCreate);

        addAllRoomsFor1(createDto.getNumberOfRoomsFor1(), createdEntity);
        addAllRoomsFor2(createDto.getNumberOfRoomsFor2(), createdEntity);
        addAllRoomsFor3(createDto.getNumberOfRoomsFor3(), createdEntity);
        addAllRoomsFor4(createDto.getNumberOfRoomsFor4(), createdEntity);

        country.getHotels().add(createdEntity);

        HotelFullDto createdDto = hotelMapper.map(createdEntity);
        log.info("Hotel was successfully created.");
        return createdDto;
    }

    @Override
    @Transactional
    public HotelFullDto update(HotelUpdateDto updateDto) {
        HotelEntity existingEntity = hotelRepository.findById(updateDto.getId())
                .orElseThrow(()->new EntityNotFoundException("HotelEntity was not found by id: " + updateDto.getId()));

        existingEntity.setName(updateDto.getName());
        existingEntity.setCountry(countryRepository.getById(updateDto.getCountryId()));
        existingEntity.setAddress(updateDto.getAddress());
        existingEntity.setPrice(updateDto.getPrice());

        HotelFullDto savedDto = hotelMapper.map(hotelRepository.save(existingEntity));
        return savedDto;
    }

    @Override
    @Transactional
    public HotelFullDto findById(int id) {
        HotelEntity foundEntity = hotelRepository.findById(id)
                .orElseThrow(()-> new EntityNotFoundException("HotelEntity was not found by id: " + id));

        HotelFullDto foundDto = hotelMapper.map(foundEntity);
        log.info("Hotel was successfully found.");
        return foundDto;
    }

    @Override
    @Transactional
    public List<HotelShortDto> findAll() {
        List<HotelEntity> foundEntities = hotelRepository.findAll();
        List<HotelShortDto> foundDtos = hotelMapper.map(foundEntities);
        log.info(foundDtos.size() + " users were found.");
        return foundDtos;
    }

    @Override
    @Transactional
    public List<RoomShortDto> findAllRoomsInHotel(Integer hotelId) {
        List<RoomShortDto> dtos = roomMapper.map(roomRepository.hotelAllRooms(hotelId));
        for(RoomShortDto dto : dtos){
            dto.setHotelId(hotelId);
        }
        return dtos;
    }

    @Override
    @Transactional
    public void delete(int id) {
        HotelEntity foundEntity = hotelRepository.findById(id)
                .orElseThrow(()-> new EntityNotFoundException("HotelEntity was not found by id: " + id));

        foundEntity.setDeletedAt(Instant.now());
        hotelRepository.save(foundEntity);
        log.info("Hotel was successfully deleted.");
    }
    
    @Override
    @Transactional
    public HotelFullDto deleteRoom(Integer hotelId, Integer numberOfPeople) {

        if(numberOfPeople<1 || numberOfPeople>4){
            throw new IllegalAmountOfPeopleException("We do not have such rooms.");
        }

        HotelEntity foundHotel = hotelRepository.findById(hotelId)
                .orElseThrow(()->new EntityNotFoundException("HotelEntity was not found by id: " + hotelId));

        List<RoomEntity> foundRooms = roomRepository.findByHotelAndGuests(hotelId,numberOfPeople);
        RoomEntity roomToDelete = foundRooms.get(0);
        roomToDelete.setDeletedAt(Instant.now());
        roomRepository.save(roomToDelete);

        if(numberOfPeople==1){
            Integer numberOfRooms = foundHotel.getNumberOfRoomsFor1();
            foundHotel.setNumberOfRoomsFor1(numberOfRooms-1);
            hotelRepository.save(foundHotel);
        }
        if(numberOfPeople==2){
            Integer numberOfRooms = foundHotel.getNumberOfRoomsFor2();
            foundHotel.setNumberOfRoomsFor2(numberOfRooms-1);
            hotelRepository.save(foundHotel);
        }
        if(numberOfPeople==3){
            Integer numberOfRooms = foundHotel.getNumberOfRoomsFor3();
            foundHotel.setNumberOfRoomsFor3(numberOfRooms-1);
            hotelRepository.save(foundHotel);
        }
        if(numberOfPeople==4){
            Integer numberOfRooms = foundHotel.getNumberOfRoomsFor4();
            foundHotel.setNumberOfRoomsFor4(numberOfRooms-1);
            hotelRepository.save(foundHotel);
        }
        hotelRepository.save(foundHotel);
        log.info("Room was successfully deleted.");
        return hotelMapper.map(foundHotel);
    }

    @Override
    @Transactional
    public HotelFullDto addRoom(Integer hotelId, Integer numberOfPeople) {
        HotelEntity foundHotel = hotelRepository.findById(hotelId)
                .orElseThrow(()->new EntityNotFoundException("HotelEntity was not found by id: " + hotelId));

        if(numberOfPeople<1 || numberOfPeople>4){
            throw new IllegalAmountOfPeopleException("We do not have such rooms.");
        }
        if(numberOfPeople==1){
            Integer numberOfRooms = foundHotel.getNumberOfRoomsFor1();
            foundHotel.setNumberOfRoomsFor1(numberOfRooms+1);
            hotelRepository.save(foundHotel);
            addRoomFor1(foundHotel);
        }
        if(numberOfPeople==2){
            Integer numberOfRooms = foundHotel.getNumberOfRoomsFor2();
            foundHotel.setNumberOfRoomsFor2(numberOfRooms+1);
            hotelRepository.save(foundHotel);
            addRoomFor2(foundHotel);
        }
        if(numberOfPeople==3){
            Integer numberOfRooms = foundHotel.getNumberOfRoomsFor3();
            foundHotel.setNumberOfRoomsFor3(numberOfRooms+1);
            hotelRepository.save(foundHotel);
            addRoomFor3(foundHotel);
        }
        if(numberOfPeople==4){
            Integer numberOfRooms = foundHotel.getNumberOfRoomsFor4();
            foundHotel.setNumberOfRoomsFor4(numberOfRooms+1);
            hotelRepository.save(foundHotel);
           addRoomFor4(foundHotel);
        }
        hotelRepository.save(foundHotel);
        log.info("Room was successfully added.");
        return hotelMapper.map(foundHotel);
    }


    private void addAllRoomsFor1(Integer numberOfRooms, HotelEntity hotel){
        for(int i = 0; i<numberOfRooms; i++){
            RoomEntity room = new RoomEntity();
            room.setHotel(hotel);
            room.setPriceForRoom(hotel.getPrice()*1);
            room.setNumberOfPeople(1);
            RoomEntity savedRoom = roomRepository.save(room);
        }
    }

    private void addAllRoomsFor2(Integer numberOfRooms, HotelEntity hotel){
        for(int i = 0; i<numberOfRooms; i++){
            RoomEntity room = new RoomEntity();
            room.setHotel(hotel);
            room.setPriceForRoom(hotel.getPrice()*1.25);
            room.setNumberOfPeople(2);
            roomRepository.save(room);
        }
    }

    private void addAllRoomsFor3(Integer numberOfRooms, HotelEntity hotel){
        for(int i = 0; i<numberOfRooms; i++){
            RoomEntity room = new RoomEntity();
            room.setHotel(hotel);
            room.setPriceForRoom(hotel.getPrice()*1.5);
            room.setNumberOfPeople(3);
            roomRepository.save(room);
        }
    }

    private void addAllRoomsFor4(Integer numberOfRooms, HotelEntity hotel){
        for(int i = 0; i<numberOfRooms; i++){
            RoomEntity room = new RoomEntity();
            room.setHotel(hotel);
            room.setPriceForRoom(hotel.getPrice()*1.75);
            room.setNumberOfPeople(4);
            roomRepository.save(room);
        }
    }

    private void addRoomFor1(HotelEntity hotel){
            RoomEntity room = new RoomEntity();
            room.setHotel(hotel);
            room.setPriceForRoom(hotel.getPrice()*1);
            room.setNumberOfPeople(1);
            roomRepository.save(room);
    }

    private void addRoomFor2(HotelEntity hotel){
            RoomEntity room = new RoomEntity();
            room.setHotel(hotel);
            room.setPriceForRoom(hotel.getPrice()*1.25);
            room.setNumberOfPeople(2);
            roomRepository.save(room);
    }

    private void addRoomFor3(HotelEntity hotel){
            RoomEntity room = new RoomEntity();
            room.setHotel(hotel);
            room.setPriceForRoom(hotel.getPrice()*1.5);
            room.setNumberOfPeople(3);
            roomRepository.save(room);
    }

    private void addRoomFor4(HotelEntity hotel){
            RoomEntity room = new RoomEntity();
            room.setHotel(hotel);
            room.setPriceForRoom(hotel.getPrice()*1.75);
            room.setNumberOfPeople(4);
            roomRepository.save(room);
    }


}
