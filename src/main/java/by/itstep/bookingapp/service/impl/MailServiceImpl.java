package by.itstep.bookingapp.service.impl;
import by.itstep.bookingapp.service.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class MailServiceImpl implements MailService {

    @Autowired
    private JavaMailSender mailSender;

    @Override
    public void sendEmail(String email, String message) {
        SimpleMailMessage mail = new SimpleMailMessage();
        mail.setSubject("Email from BookingApp Team");
        mail.setTo(email);
        mail.setText(message);
        mailSender.send(mail);
    }

}
