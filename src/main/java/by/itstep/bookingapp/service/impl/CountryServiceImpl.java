package by.itstep.bookingapp.service.impl;
import by.itstep.bookingapp.dto.country.CountryCreateDto;
import by.itstep.bookingapp.dto.country.CountryFullDto;
import by.itstep.bookingapp.entities.CountryEntity;
import by.itstep.bookingapp.exception.UniqueValueAlreadyTakenException;
import by.itstep.bookingapp.mapper.CountryMapper;
import by.itstep.bookingapp.repository.CountryRepository;
import by.itstep.bookingapp.service.CountryService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityNotFoundException;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class CountryServiceImpl implements CountryService {

    private final CountryMapper countryMapper;
    private final CountryRepository countryRepository;


    @Override
    @Transactional
    public CountryFullDto create(CountryCreateDto dto) {
        Optional<CountryEntity> entityWithSameName = countryRepository.findByName(dto.getName());
        if(entityWithSameName.isPresent()){
            throw new UniqueValueAlreadyTakenException("Such country already exists!");
        }
        CountryEntity entityToCreate = countryMapper.map(dto);
        entityToCreate.setName(dto.getName());
        CountryEntity createdEntity = countryRepository.save(entityToCreate);
        CountryFullDto createdFullDto = countryMapper.map(createdEntity);
        log.info("Country was successfully created.");
        return createdFullDto;
    }

    @Override
    @Transactional
    public CountryFullDto findById(int id) {
        CountryEntity foundEntity = countryRepository.findById(id)
                .orElseThrow(()->new EntityNotFoundException("CountryEntity was not found by id: " + id));

        CountryFullDto foundDto = countryMapper.map(foundEntity);
        log.info("Country was successfully found.");
        return foundDto;
    }

    @Override
    @Transactional
    public List<CountryFullDto> findAll() {
        List<CountryEntity> foundEntities = countryRepository.findAll();
        List<CountryFullDto> foundDtos = countryMapper.map(foundEntities);
        log.info(foundDtos.size() + " countries were found.");
        return foundDtos;
    }

    @Override
    @Transactional
    public void delete(int id) {
        CountryEntity foundEntity = countryRepository.findById(id)
                .orElseThrow(()->new EntityNotFoundException("CountryEntity was not found by id: " + id));

        foundEntity.setDeletedAt(Instant.now());
        countryRepository.save(foundEntity);
        log.info("country was successfully deleted.");
    }
}
