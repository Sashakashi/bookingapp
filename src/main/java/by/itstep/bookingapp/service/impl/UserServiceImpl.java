package by.itstep.bookingapp.service.impl;
import by.itstep.bookingapp.dto.user.*;
import by.itstep.bookingapp.entities.UserEntity;
import by.itstep.bookingapp.entities.UserRole;
import by.itstep.bookingapp.exception.HaveNoRightsException;
import by.itstep.bookingapp.exception.UniqueValueAlreadyTakenException;
import by.itstep.bookingapp.exception.WrongUserPasswordException;
import by.itstep.bookingapp.mapper.UserMapper;
import by.itstep.bookingapp.repository.UserRepository;
import by.itstep.bookingapp.security.AuthenticationService;
import by.itstep.bookingapp.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityNotFoundException;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserMapper userMapper;
    private final UserRepository userRepository;
    private final AuthenticationService authenticationService;
    private final PasswordEncoder passwordEncoder;


    @Override
    @Transactional
    public UserFullDto create(UserCreateDto createDto) {
        UserEntity entityToCreate = userMapper.map(createDto);
        Optional<UserEntity> entityWithSameEmail = userRepository.findByEmail(createDto.getEmail());

        if(entityWithSameEmail.isPresent()){
            throw new UniqueValueAlreadyTakenException("Such email is already taken!");
        }

        entityToCreate.setBalance(0.0);
        entityToCreate.setRole(UserRole.USER);
        entityToCreate.setBlocked(false);

        entityToCreate.setPassword(passwordEncoder.encode(createDto.getPassword()+createDto.getEmail()));

        UserEntity createdEntity = userRepository.save(entityToCreate);

        UserFullDto createdDto = userMapper.map(createdEntity);
        log.info("User was successfully created.");
        return createdDto;
    }

    @Override
    @Transactional
    public UserFullDto findById(int id){
        UserEntity foundEntity = userRepository.findById(id)
                .orElseThrow(()->new EntityNotFoundException("UserEntity was not found by id: " + id));

        UserFullDto foundDto = userMapper.map(foundEntity);
        log.info("User was successfully found.");
        return foundDto;
    }

    @Override
    @Transactional
    public List<UserShortDto> findAll() {
        List<UserEntity> foundEntities = userRepository.findAll();
        List<UserShortDto> foundDtos = userMapper.map(foundEntities);
        log.info(foundDtos.size() + " users were found.");
        return foundDtos;
    }

    @Override
    @Transactional
    public UserFullDto update(UserUpdateDto updateDto) {
        UserEntity currentUser = authenticationService.getAuthenticatedUser();

        UserEntity existingEntity = userRepository.findById(updateDto.getId())
                .orElseThrow(()->new EntityNotFoundException("UserEntity was not found by id: " + updateDto.getId()));

        if(currentUser != existingEntity && currentUser.getRole() != UserRole.ADMIN){
            throw new HaveNoRightsException("You can not update another user!");
        }

        existingEntity.setName(updateDto.getName());
        existingEntity.setLastname(updateDto.getLastname());
        existingEntity.setPhone(updateDto.getPhone());

        UserFullDto savedDto = userMapper.map(userRepository.save(existingEntity));
        return savedDto;
    }

    @Override
    @Transactional
    public void delete(int id) {
    UserEntity currentUser = authenticationService.getAuthenticatedUser();

    UserEntity foundEntity = userRepository.findById(id)
            .orElseThrow(()->new EntityNotFoundException("UserEntity was not found by id: " + id));

    if(currentUser != foundEntity && currentUser.getRole() != UserRole.ADMIN){
       throw new HaveNoRightsException("You can not delete another user!");
    }

    foundEntity.setDeletedAt(Instant.now());
    userRepository.save(foundEntity);
    log.info("User was successfully deleted.");
    }

    @Override
    public UserFullDto changePassword(ChangeUserPasswordDto passwordDto) {
        UserEntity currentUser = authenticationService.getAuthenticatedUser();

        UserEntity entityToChangePassword = userRepository.findById(passwordDto.getId())
                .orElseThrow(()->new EntityNotFoundException("UserEntity was not found by id: " + passwordDto.getId()));

        if(currentUser != entityToChangePassword && currentUser.getRole() != UserRole.ADMIN){
            throw new HaveNoRightsException("You can not update another user!");
        }

        if(!passwordEncoder.matches(passwordDto.getOldPassword()+entityToChangePassword.getEmail(),
                                                entityToChangePassword.getPassword())){
            throw new WrongUserPasswordException("Wrong password.");
        }

        entityToChangePassword.setPassword(
                passwordEncoder.encode(passwordDto.getNewPassword()+entityToChangePassword.getEmail()));
        UserEntity changedUser = userRepository.save(entityToChangePassword);
        log.info("User password was successfully updated.");
        return userMapper.map(changedUser);
    }

    @Override
    public UserFullDto changeRole(ChangeUserRoleDto roleDto) {

        UserEntity entityToChange = userRepository.findById(roleDto.getId())
                .orElseThrow(()->new EntityNotFoundException("UserEntity was not found by id: " + roleDto.getId()));

        entityToChange.setRole(roleDto.getNewRole());
        UserEntity changedEntity = userRepository.save(entityToChange);
        UserFullDto changedDto = userMapper.map(changedEntity);
        log.info("User role was successfully updated.");
        return changedDto;
    }

    @Override
    public void boostBalance(BoostBalanceDto dto) {
        UserEntity currentUser = authenticationService.getAuthenticatedUser();

        UserEntity entityToChange = userRepository.findById(dto.getId())
                .orElseThrow(()->new EntityNotFoundException("UserEntity was not found by id: " + dto.getId()));

        if(currentUser != entityToChange && currentUser.getRole() != UserRole.ADMIN){
            throw new HaveNoRightsException("You can not boost another user's balance!");
        }

        entityToChange.setBalance(dto.getNewBalance());
        UserEntity changedEntity = userRepository.save(entityToChange);
        log.info("User's balance was successfully boosted. Your current balance: " + changedEntity.getBalance());
    }


}
