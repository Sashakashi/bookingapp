package by.itstep.bookingapp.service.impl;

import by.itstep.bookingapp.dto.booking.BookingCreateDto;
import by.itstep.bookingapp.dto.booking.BookingFullDto;
import by.itstep.bookingapp.entities.*;
import by.itstep.bookingapp.exception.DoNotHaveAvailableOptionException;
import by.itstep.bookingapp.exception.HaveNoRightsException;
import by.itstep.bookingapp.exception.NotEnoughMoneyException;
import by.itstep.bookingapp.mapper.BookingMapper;
import by.itstep.bookingapp.repository.BookingRepository;
import by.itstep.bookingapp.repository.RoomRepository;
import by.itstep.bookingapp.repository.UserRepository;
import by.itstep.bookingapp.security.AuthenticationService;
import by.itstep.bookingapp.service.BookingService;
import by.itstep.bookingapp.service.MailService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityNotFoundException;
import java.sql.Date;
import java.time.Instant;
import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class BookingServiceImpl implements BookingService {

    private final BookingMapper bookingMapper;
    private final RoomRepository roomRepository;
    private final UserRepository userRepository;
    private final BookingRepository bookingRepository;
    private final AuthenticationService authenticationService;
    private final MailService mailService;


    @Override
    @Transactional
    public BookingFullDto  createAndPay(BookingCreateDto dto){
        UserEntity currentUser = authenticationService.getAuthenticatedUser();

        if(currentUser != userRepository.getById(dto.getUserId()) && currentUser.getRole() != UserRole.ADMIN){
            throw new HaveNoRightsException("You can not create another user's booking!");
        }

        RoomEntity foundRoom = findAvailableRoom(dto);

        BookingEntity bookingToCreate = bookingMapper.map(dto);
        bookingToCreate.setNumberOfPeople(dto.getNumberOfPeople());
        bookingToCreate.setDateIn(dto.getDateIn());
        bookingToCreate.setDateOut(dto.getDateOut());
        bookingToCreate.setRoom(foundRoom);
        UserEntity user = userRepository.findById(dto.getUserId())
                .orElseThrow(()-> new EntityNotFoundException("User was not found by id: "
                                                              + userRepository.findById(dto.getUserId())));

        bookingToCreate.setUser(user);

        foundRoom.getBookings().add(bookingToCreate);

        Double totalPrice = countTotalPrice(dto.getDateIn(), dto.getDateOut(), foundRoom);

        if(user.getBalance()>=totalPrice){
            user.setBalance(user.getBalance() - totalPrice);
            userRepository.save(user);
        }else{
            throw new NotEnoughMoneyException("Boost your balance!");
        }

        BookingEntity createdBooking = bookingRepository.save(bookingToCreate);
        log.info("Booking was successfully paid and created.");
        mailService.sendEmail("sashakashi@mail.ru",
                            "You have booked a room in hotel "+bookingToCreate.getRoom().getHotel().getName());
        return bookingMapper.map(createdBooking);
    }


    @Override
    @Transactional
    public BookingFullDto findById(Integer id) {
        BookingEntity foundEntity = bookingRepository.findById(id)
                .orElseThrow(()->new EntityNotFoundException("BookingEntity was not found by id: " + id));

        BookingFullDto foundDto = bookingMapper.map(foundEntity);
        log.info("Booking was successfully found.");
        return foundDto;
    }

    @Override
    public List<BookingFullDto> findAll() {
        List<BookingEntity> foundEntities = bookingRepository.findAll();
        List<BookingFullDto> foundDtos = bookingMapper.map(foundEntities);
        log.info(foundDtos.size() + " bookings were found.");
        return foundDtos;
    }

    @Override
    @Transactional
    public void deleteAndRefund(Integer id) {
        UserEntity currentUser = authenticationService.getAuthenticatedUser();

        BookingEntity foundEntity = bookingRepository.findById(id)
                .orElseThrow(()->new EntityNotFoundException("BookingEntity was not found by id: " + id));

        if(currentUser.getId() != foundEntity.getUser().getId() && currentUser.getRole() != UserRole.ADMIN){
            throw new HaveNoRightsException("You can not create another user's booking!");
        }

        refund(foundEntity);

        foundEntity.setDeletedAt(Instant.now());
        bookingRepository.save(foundEntity);
        mailService.sendEmail("sashakashi@mail.ru",
                        "You have cancelled your booking in hotel "+foundEntity.getRoom().getHotel().getName());
        log.info("Booking was successfully deleted. Money was returned to user " + foundEntity.getUser().getLastname());
    }



    private RoomEntity findAvailableRoom(BookingCreateDto dto){
        List<RoomEntity> foundRooms = roomRepository.findByHotelAndGuests(dto.getHotelId(), dto.getNumberOfPeople());
        RoomEntity foundAvailableRoom = null;
        boolean found = false;

        for(RoomEntity room : foundRooms){
            found = true;
            for(BookingEntity booking : room.getBookings()){
                if (dto.getDateIn().before(booking.getDateOut()) && dto.getDateOut().after(booking.getDateIn())) {
                    found = false;
                    break;
                }
            }
            if(found) {
                foundAvailableRoom=room;
                break;
            }
        }
        if(!found){
            throw new DoNotHaveAvailableOptionException("Unfortunately, we don't have available room.");
        }
        return foundAvailableRoom;
    }


    private double countTotalPrice(Date dateIn, Date dateOut, RoomEntity foundRoom){
        Integer numberOfDays = (int)((dateOut.getTime()-dateIn.getTime())/86400000);
        return numberOfDays*foundRoom.getPriceForRoom();
    }


    private void refund(BookingEntity booking){
        Double totalPrice = countTotalPrice(booking.getDateIn(),booking.getDateOut(),booking.getRoom());
        UserEntity user = userRepository.findById(booking.getUser().getId())
                        .orElseThrow(()->new EntityNotFoundException("UserEntity was not found for refund by id: "
                        + booking.getUser().getId()));
        user.setBalance(user.getBalance()  +  totalPrice);
        userRepository.save(user);
    }

}
