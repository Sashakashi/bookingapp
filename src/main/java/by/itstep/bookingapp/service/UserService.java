package by.itstep.bookingapp.service;
import by.itstep.bookingapp.dto.user.*;
import java.util.List;

public interface UserService {

    UserFullDto create(UserCreateDto dto);

    UserFullDto update(UserUpdateDto dto);

    UserFullDto findById(int id);

    List<UserShortDto> findAll();

    void delete(int id);

    UserFullDto changePassword(ChangeUserPasswordDto dto);

    UserFullDto changeRole(ChangeUserRoleDto dto);

    void boostBalance(BoostBalanceDto dto);
}
