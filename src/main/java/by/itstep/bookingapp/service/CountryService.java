package by.itstep.bookingapp.service;
import by.itstep.bookingapp.dto.country.CountryCreateDto;
import by.itstep.bookingapp.dto.country.CountryFullDto;
import java.util.List;

public interface CountryService {

    CountryFullDto create(CountryCreateDto dto);

    CountryFullDto findById(int id);

    List<CountryFullDto> findAll();

    void delete(int id);

}
