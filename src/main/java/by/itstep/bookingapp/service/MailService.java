package by.itstep.bookingapp.service;

public interface MailService {

    void sendEmail(String email, String message);

}
