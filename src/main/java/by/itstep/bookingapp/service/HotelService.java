package by.itstep.bookingapp.service;
import by.itstep.bookingapp.dto.hotel.HotelCreateDto;
import by.itstep.bookingapp.dto.hotel.HotelFullDto;
import by.itstep.bookingapp.dto.hotel.HotelShortDto;
import by.itstep.bookingapp.dto.hotel.HotelUpdateDto;
import by.itstep.bookingapp.dto.room.RoomShortDto;
import java.util.List;

public interface HotelService {

    HotelFullDto create(HotelCreateDto dto);

    HotelFullDto update(HotelUpdateDto dto);

    HotelFullDto findById(int id);

    List<HotelShortDto> findAll();

    List<RoomShortDto> findAllRoomsInHotel(Integer hotelId);

    void delete(int id);

    HotelFullDto deleteRoom(Integer hotelId, Integer numberOfPeople);

    HotelFullDto addRoom(Integer hotelId, Integer numberOfPeople);
}
