package by.itstep.bookingapp.repository;
import by.itstep.bookingapp.entities.CountryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.Optional;

public interface CountryRepository extends JpaRepository <CountryEntity, Integer> {

    Optional<CountryEntity> findByName (String name);
}
