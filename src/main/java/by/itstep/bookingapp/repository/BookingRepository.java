package by.itstep.bookingapp.repository;
import by.itstep.bookingapp.dto.booking.BookingFullDto;
import by.itstep.bookingapp.entities.BookingEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import java.util.List;

public interface BookingRepository extends JpaRepository <BookingEntity, Integer> {

    @Query(value = "SELECT * FROM bookings WHERE user_id=:userId AND deleted_at IS NULL", nativeQuery = true)
    List<BookingFullDto> findByUser(@Param("userId") Integer userId);

    @Query(value = "SELECT * FROM bookings JOIN rooms ON rooms.id=bookings.room_id \n" +
            "WHERE rooms.hotel_id=:hotelId AND bookings.deleted_at IS NULL;", nativeQuery = true)
    List<BookingFullDto> findByHotel(@Param("hotelId") Integer hotelId);

    @Query(value = "SELECT * FROM bookings WHERE room_id=:roomId AND deleted_at IS NULL", nativeQuery = true)
    List<BookingFullDto> findByRoom(@Param("roomId")Integer roomId);


}
