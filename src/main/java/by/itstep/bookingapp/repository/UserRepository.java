package by.itstep.bookingapp.repository;
import by.itstep.bookingapp.entities.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import java.util.Optional;

public interface UserRepository extends JpaRepository <UserEntity, Integer> {

    Optional<UserEntity> findByEmail (String email);

    @Query(value = "UPDATE users SET balance =:sum WHERE id =:userId AND deleted_at IS NULL ", nativeQuery = true)
    UserEntity boostBalance(@Param("sum") Double sum, @Param("userId") Integer userId);

}
