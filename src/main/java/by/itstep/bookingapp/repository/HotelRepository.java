package by.itstep.bookingapp.repository;
import by.itstep.bookingapp.dto.hotel.HotelShortDto;
import by.itstep.bookingapp.entities.HotelEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import java.util.List;

public interface HotelRepository extends JpaRepository <HotelEntity, Integer> {

    @Query(value = "SELECT * FROM hotels WHERE country_id=:countryId AND deleted_at IS NULL", nativeQuery = true)
    List<HotelShortDto> findByCountry(@Param("countryId") Integer countryId);

}
