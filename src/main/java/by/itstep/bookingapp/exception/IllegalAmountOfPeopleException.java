package by.itstep.bookingapp.exception;

public class IllegalAmountOfPeopleException extends RuntimeException{

    public IllegalAmountOfPeopleException(String message){
        super(message);
    }
}
