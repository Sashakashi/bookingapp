package by.itstep.bookingapp.exception;

public class HaveNoRightsException  extends RuntimeException{

    public HaveNoRightsException(String message){
        super(message);
    }
}
