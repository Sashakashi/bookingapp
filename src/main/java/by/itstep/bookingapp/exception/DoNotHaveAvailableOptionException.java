package by.itstep.bookingapp.exception;

public class DoNotHaveAvailableOptionException extends RuntimeException{

    public DoNotHaveAvailableOptionException(String message){
        super(message);
    }
}
