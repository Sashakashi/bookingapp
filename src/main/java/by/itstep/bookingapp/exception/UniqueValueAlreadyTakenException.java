package by.itstep.bookingapp.exception;

public class UniqueValueAlreadyTakenException extends RuntimeException{

    public UniqueValueAlreadyTakenException(String message){
        super(message);
    }
}
