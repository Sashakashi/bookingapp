package by.itstep.bookingapp.utils;
import by.itstep.bookingapp.dto.country.CountryCreateDto;
import by.itstep.bookingapp.dto.country.CountryFullDto;
import by.itstep.bookingapp.dto.hotel.HotelCreateDto;
import by.itstep.bookingapp.dto.hotel.HotelUpdateDto;
import by.itstep.bookingapp.service.CountryService;
import by.itstep.bookingapp.service.HotelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import static by.itstep.bookingapp.BookingappApplicationTests.FAKER;

@Component
public class DbHotelCreator {

    @Autowired
    private HotelService hotelService;
    @Autowired
    private CountryService countryService;

    public HotelCreateDto generateCreateDto(){
        HotelCreateDto createDto = new HotelCreateDto();

        CountryCreateDto forCreateDto = new CountryCreateDto();
        forCreateDto.setName("test country" + Math.random());
        CountryFullDto createdCountry = countryService.create(forCreateDto);
        createDto.setCountryId(createdCountry.getId());

        createDto.setName(FAKER.name().title());
        createDto.setAddress(FAKER.address().streetAddress());
        createDto.setPrice(5.0);
        createDto.setNumberOfRoomsFor1(1);
        createDto.setNumberOfRoomsFor2(2);
        createDto.setNumberOfRoomsFor3(2);
        createDto.setNumberOfRoomsFor4(2);

        return createDto;
    }

    public HotelUpdateDto generateUpdateDto(Integer hotelId, Integer countryId){
        HotelUpdateDto updateDto = new HotelUpdateDto();
        updateDto.setId(hotelId);
        updateDto.setCountryId(countryId);
        updateDto.setName(FAKER.name().title());
        updateDto.setAddress(FAKER.address().streetAddress());
        updateDto.setPrice(7.0);
        return updateDto;
    }
}
