package by.itstep.bookingapp.utils;
import by.itstep.bookingapp.dto.user.*;
import by.itstep.bookingapp.entities.UserEntity;
import by.itstep.bookingapp.entities.UserRole;
import by.itstep.bookingapp.mapper.UserMapper;
import by.itstep.bookingapp.repository.UserRepository;
import by.itstep.bookingapp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import static by.itstep.bookingapp.BookingappApplicationTests.FAKER;

@Component
public class DbUserCreator {

    @Autowired
    private UserService userService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserMapper userMapper;

    public UserCreateDto generateCreateDto(){
        UserCreateDto createDto = new UserCreateDto();
        createDto.setName(FAKER.name().firstName());
        createDto.setLastname(FAKER.name().lastName());
        createDto.setEmail("test email"+Math.random());
        createDto.setPassword(FAKER.internet().password());
        createDto.setPhone(FAKER.phoneNumber().cellPhone());
        return createDto;
    }

    public UserUpdateDto generateUpdateDto(Integer userId){
        UserUpdateDto updateDto = new UserUpdateDto();
        updateDto.setId(userId);
        updateDto.setName(FAKER.name().firstName());
        updateDto.setLastname(FAKER.name().lastName());
        updateDto.setPhone(FAKER.phoneNumber().cellPhone());
        return updateDto;
    }

    public BoostBalanceDto generateBalanceDto(Integer userId, String email, String password){
        BoostBalanceDto dto = new BoostBalanceDto();
        dto.setId(userId);
        dto.setEmail(email);
        dto.setPassword(password);
        dto.setNewBalance(1000.0);
        return dto;
    }

    public ChangeUserRoleDto generateRoleDto(Integer userId){
        ChangeUserRoleDto dto = new ChangeUserRoleDto();
        dto.setId(userId);
        dto.setNewRole(UserRole.ADMIN);
        return dto;
    }

    public ChangeUserPasswordDto generateChangePasswordDto(Integer userId, String oldPassword){
        ChangeUserPasswordDto dto = new ChangeUserPasswordDto();
        dto.setId(userId);
        dto.setOldPassword(oldPassword);
        dto.setNewPassword(FAKER.internet().password());
        return dto;
    }

    public UserFullDto addAdminToDb(){
        UserCreateDto userCreateDto = generateCreateDto();
        UserFullDto userDto = userService.create(userCreateDto);
        UserEntity user = userRepository.getById(userDto.getId());
        user.setRole(UserRole.ADMIN);
        return userMapper.map(userRepository.save(user));
    }
}
