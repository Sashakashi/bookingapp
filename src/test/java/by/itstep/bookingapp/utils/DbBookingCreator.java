package by.itstep.bookingapp.utils;
import by.itstep.bookingapp.dto.booking.BookingCreateDto;
import by.itstep.bookingapp.entities.UserEntity;
import by.itstep.bookingapp.repository.UserRepository;
import by.itstep.bookingapp.service.HotelService;
import by.itstep.bookingapp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.sql.Date;
import java.time.LocalDate;

@Component
public class DbBookingCreator {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserService userService;
    @Autowired
    private DbUserCreator dbUserCreator;
    @Autowired
    private HotelService hotelService;
    @Autowired
    private DbHotelCreator dbHotelCreator;

    public BookingCreateDto generateBookingCreateDto(){
        BookingCreateDto dto = new BookingCreateDto();
        UserEntity user = userRepository.getById(userService.create(dbUserCreator.generateCreateDto()).getId());

        user.setBalance(1000.0);

        UserEntity updatedUser = userRepository.save(user);
        dto.setUserId(updatedUser.getId());
        dto.setHotelId(hotelService.create(dbHotelCreator.generateCreateDto()).getId());
        dto.setNumberOfPeople(1);
        dto.setDateIn(Date.valueOf(LocalDate.now().plusDays(3)));
        dto.setDateOut(Date.valueOf(LocalDate.now().plusDays(5)));
        return dto;
    }

    public BookingCreateDto generateBookingCreateDtoWithNoMoney(){
        BookingCreateDto dto = new BookingCreateDto();
        UserEntity user = userRepository.getById(userService.create(dbUserCreator.generateCreateDto()).getId());
        dto.setUserId(user.getId());
        dto.setHotelId(hotelService.create(dbHotelCreator.generateCreateDto()).getId());
        dto.setNumberOfPeople(1);
        dto.setDateIn(Date.valueOf(LocalDate.now().plusDays(3)));
        dto.setDateOut(Date.valueOf(LocalDate.now().plusDays(5)));
        return dto;
    }
}
