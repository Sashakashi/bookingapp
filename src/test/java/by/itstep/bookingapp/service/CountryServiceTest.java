package by.itstep.bookingapp.service;
import by.itstep.bookingapp.BookingappApplicationTests;
import by.itstep.bookingapp.dto.country.CountryCreateDto;
import by.itstep.bookingapp.dto.country.CountryFullDto;
import by.itstep.bookingapp.entities.CountryEntity;
import by.itstep.bookingapp.exception.UniqueValueAlreadyTakenException;
import by.itstep.bookingapp.repository.BookingRepository;
import by.itstep.bookingapp.repository.CountryRepository;
import by.itstep.bookingapp.repository.HotelRepository;
import by.itstep.bookingapp.repository.RoomRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Commit;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityNotFoundException;
import java.time.Instant;
import java.util.List;

public class CountryServiceTest extends BookingappApplicationTests {

    @Autowired
    private CountryRepository countryRepository;
    @Autowired
    private CountryService countryService;
    @Autowired
    private HotelRepository hotelRepository;
    @Autowired
    private RoomRepository roomRepository;
    @Autowired
    private BookingRepository bookingRepository;

    @Test
    @Transactional
    @Commit
    public void create_happyPath(){
        //given
        CountryCreateDto createDto = new CountryCreateDto();
        createDto.setName("test country name");
        CountryFullDto createdCountryFullDto = countryService.create(createDto);
        //when
        CountryEntity foundCreatedCountry = countryRepository.getById(createdCountryFullDto.getId());
        //then
        Assertions.assertNotNull(foundCreatedCountry);
        Assertions.assertEquals(createDto.getName(), foundCreatedCountry.getName());
        System.out.println(foundCreatedCountry.getId());
    }

    @Test
    @Transactional
    public void create_whenCountryAlreadyExists(){
        //given
        CountryCreateDto createDto = new CountryCreateDto();
        createDto.setName("test country name");
        countryService.create(createDto);
        CountryCreateDto createDto2 = new CountryCreateDto();
        createDto2.setName(createDto.getName());
        //when
        //then
        Assertions.assertThrows(UniqueValueAlreadyTakenException.class, () -> countryService.create(createDto2));
    }

    @Test
    @Transactional
    public void findById_happyPath(){
        //given
        CountryCreateDto createDto = new CountryCreateDto();
        createDto.setName("test country name");
        CountryFullDto createdCountryFullDto = countryService.create(createDto);
        //when
        CountryFullDto foundByIdDto = countryService.findById(createdCountryFullDto.getId());
        //then
        Assertions.assertNotNull(foundByIdDto);
        Assertions.assertEquals(createDto.getName(), foundByIdDto.getName());
    }

    @Test
    @Transactional
    public void findById_WhenNoOneFound(){
        //given
        //when
        //then
        Assertions.assertThrows(EntityNotFoundException.class, ()->countryService.findById(2));
    }

    @Test
    @Transactional
    @Commit
    public void findAll_happyPath(){
        //given
        CountryCreateDto createDto = new CountryCreateDto();
        createDto.setName("test country name");
        countryService.create(createDto);
        CountryCreateDto createDto2 = new CountryCreateDto();
        createDto2.setName("test country name2");
        countryService.create(createDto2);
        //when
        List<CountryFullDto> foundDtos = countryService.findAll();
        //then
        Assertions.assertEquals(2, foundDtos.size());
    }

    @Test
    @Transactional
    @Commit
    public void findAll_whenOneIsDeleted(){
        //given
        CountryCreateDto createDto = new CountryCreateDto();
        createDto.setName("test country name");
        countryService.create(createDto);
        CountryCreateDto createDto2 = new CountryCreateDto();
        createDto2.setName("test country name2");
        CountryFullDto createdCountryFullDto2 = countryService.create(createDto2);
        countryRepository.getById(createdCountryFullDto2.getId()).setDeletedAt(Instant.now());
        //when
        List<CountryFullDto> foundDtos = countryService.findAll();
        //then
        Assertions.assertEquals(1, foundDtos.size());
    }

    @Test
    @Transactional
    public void delete_happyPath(){
        //given
        CountryCreateDto createDto = new CountryCreateDto();
        createDto.setName("test country name");
        CountryFullDto createdCountryFullDto = countryService.create(createDto);
        countryService.delete(createdCountryFullDto.getId());
        //when
        CountryEntity deletedEntity = countryRepository.getById(createdCountryFullDto.getId());
        //then
        Assertions.assertNotNull(deletedEntity.getDeletedAt());
    }


}
