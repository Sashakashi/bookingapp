package by.itstep.bookingapp.service;
import by.itstep.bookingapp.BookingappApplicationTests;
import by.itstep.bookingapp.dto.user.*;
import by.itstep.bookingapp.entities.UserEntity;
import by.itstep.bookingapp.entities.UserRole;
import by.itstep.bookingapp.exception.UniqueValueAlreadyTakenException;
import by.itstep.bookingapp.repository.BookingRepository;
import by.itstep.bookingapp.repository.UserRepository;
import by.itstep.bookingapp.utils.DbUserCreator;
import by.itstep.bookingapp.utils.JwtHelper;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.annotation.Commit;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityNotFoundException;
import java.time.Instant;
import java.util.List;
import static org.springframework.http.HttpMethod.DELETE;
import static org.springframework.http.HttpMethod.PUT;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.request;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
public class UserServiceTest extends BookingappApplicationTests {

    @Autowired
    private BookingRepository bookingRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserService userService;
    @Autowired
    private DbUserCreator dbUserCreator;
    @Autowired
    private JwtHelper jwtHelper;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Test
    @Transactional
    @Commit
    public void create_happyPath(){
        //given
        UserCreateDto createDto = dbUserCreator.generateCreateDto();
        UserFullDto createdUserFullDto = userService.create(createDto);
        //when
        UserEntity foundCreatedUser = userRepository.getById(createdUserFullDto.getId());
        //then
        Assertions.assertNotNull(foundCreatedUser);
        Assertions.assertEquals(createDto.getName(), foundCreatedUser.getName());
        Assertions.assertEquals(createDto.getLastname(), foundCreatedUser.getLastname());
       if(passwordEncoder.matches(createDto.getPassword()+createDto.getEmail(), foundCreatedUser.getPassword())){
           Assertions.assertTrue(1==1);
       }else{
           Assertions.assertTrue(1==2);
       }
        Assertions.assertEquals(createDto.getEmail(), foundCreatedUser.getEmail());
        Assertions.assertEquals(createDto.getPhone(), foundCreatedUser.getPhone());
        Assertions.assertEquals(UserRole.USER, foundCreatedUser.getRole() );
    }

    @Test
    @Transactional
    public void create_WhenDuplicateEmail(){
        //given
        UserCreateDto createDto = dbUserCreator.generateCreateDto();
        UserFullDto createdFullDto = userService.create(createDto);
        UserCreateDto dtoWithSameEmail = dbUserCreator.generateCreateDto();
        dtoWithSameEmail.setEmail(createdFullDto.getEmail());
        //when
        //then
        Assertions.assertThrows(UniqueValueAlreadyTakenException.class, ()-> userService.create(dtoWithSameEmail));
    }

    @Test
    @Transactional
    public void findById_happyPath(){
        //given
        UserCreateDto createDto = dbUserCreator.generateCreateDto();
        UserFullDto createdUserFullDto = userService.create(createDto);
        //when
        UserFullDto foundByIdDto = userService.findById(createdUserFullDto.getId());
        //then
        Assertions.assertNotNull(foundByIdDto);
        Assertions.assertEquals(createDto.getName(), foundByIdDto.getName());
        Assertions.assertEquals(createDto.getLastname(), foundByIdDto.getLastname());
        Assertions.assertEquals(createDto.getEmail(), foundByIdDto.getEmail());
        Assertions.assertEquals(createDto.getPhone(), foundByIdDto.getPhone());
    }

    @Test
    @Transactional
    public void findById_WhenNoOneFound(){
        //given
        //when
        //then
        Assertions.assertThrows(EntityNotFoundException.class, ()->userService.findById(2));
    }

    @Test
    @Transactional
    @Commit
    public void findAll_happyPath(){
        //given
        UserCreateDto createDto = dbUserCreator.generateCreateDto();
        userService.create(createDto);
        UserCreateDto createDto2 = dbUserCreator.generateCreateDto();
        userService.create(createDto2);
        //when
        List<UserShortDto> foundDtos = userService.findAll();
        //then
        Assertions.assertEquals(2, foundDtos.size());
    }

    @Test
    @Transactional
    @Commit
    public void findAll_whenOneIsDeleted(){
        //given
        UserCreateDto createDto = dbUserCreator.generateCreateDto();
        userService.create(createDto);
        UserCreateDto createDto2 = dbUserCreator.generateCreateDto();
        UserFullDto createdUserDto2 = userService.create(createDto2);
        userRepository.getById(createdUserDto2.getId()).setDeletedAt(Instant.now());
        //when
        List<UserShortDto> foundDtos = userService.findAll();
        //then
        Assertions.assertEquals(1, foundDtos.size());
    }

    @Test
    @Transactional
    public void update_happyPath() throws Exception{
        //given
        UserCreateDto createDto = dbUserCreator.generateCreateDto();
        UserFullDto createdDto = userService.create(createDto);
        UserUpdateDto updateDto = dbUserCreator.generateUpdateDto(createdDto.getId());

        UserFullDto admin = dbUserCreator.addAdminToDb();
        String token = jwtHelper.createToken(admin.getEmail());

        //when
        mockMvc.perform(request(PUT, "/users")
               .header("Authorization", token)
               .header("Content-Type", "application/json")
               .content(objectMapper.writeValueAsString(updateDto)))
               .andExpect(status().isOk());

        UserFullDto foundDto = userService.findById(updateDto.getId());
        //then
        Assertions.assertNotNull(foundDto);
        Assertions.assertEquals(updateDto.getName(), foundDto.getName());
        Assertions.assertEquals(updateDto.getLastname(), foundDto.getLastname());
        Assertions.assertEquals(updateDto.getPhone(), foundDto.getPhone());
    }

    @Test
    @Transactional
    public void update_WhenEntityDoesNotExist(){
        //given
        UserUpdateDto updateDto = dbUserCreator.generateUpdateDto(2);
        //when
        //then
        Assertions.assertThrows(EntityNotFoundException.class, ()->userService.update(updateDto));
    }

    @Test
    @Transactional
    public void delete_happyPath()throws Exception{
        //given
        UserCreateDto createDto = dbUserCreator.generateCreateDto();
        UserFullDto createdDto = userService.create(createDto);

        UserFullDto admin = dbUserCreator.addAdminToDb();
        String token = jwtHelper.createToken(admin.getEmail());
        //when
        mockMvc.perform(request(DELETE, "/users/"+createdDto.getId())
               .header("Authorization", token)
               .header("Content-Type", "application/json"))
               .andExpect(status().isOk());

        UserEntity deletedEntity = userRepository.getById(createdDto.getId());
        //then
        Assertions.assertNotNull(deletedEntity.getDeletedAt());
    }

    @Test
    @Transactional
    public void changePassword_happyPath()throws Exception{
        //given
        UserCreateDto createDto = dbUserCreator.generateCreateDto();
        UserFullDto createdDto = userService.create(createDto);
        ChangeUserPasswordDto passwordDto = dbUserCreator.generateChangePasswordDto(createdDto.getId(),
                                                                                    createDto.getPassword());
        UserFullDto admin = dbUserCreator.addAdminToDb();
        String token = jwtHelper.createToken(admin.getEmail());

        //when
        mockMvc.perform(request(PUT, "/users/password")
               .header("Authorization", token)
               .header("Content-Type", "application/json")
               .content(objectMapper.writeValueAsString(passwordDto)))
               .andExpect(status().isOk());

        UserEntity foundUser = userRepository.getById(passwordDto.getId());
        //then
        if(passwordEncoder.matches(passwordDto.getNewPassword()+createdDto.getEmail(), foundUser.getPassword())){
            Assertions.assertTrue(1==1);
        }else {
            Assertions.assertTrue(1==2);
        }
    }

    @Test
    @Transactional
    public void changePassword_whenOldPasswordDoesNotMatch()throws Exception{
        //given
        UserCreateDto createDto = dbUserCreator.generateCreateDto();
        UserFullDto createdDto = userService.create(createDto);
        ChangeUserPasswordDto passwordDto = dbUserCreator.generateChangePasswordDto(createdDto.getId(),
                                                                                    "123456789");
        UserFullDto admin = dbUserCreator.addAdminToDb();
        String token = jwtHelper.createToken(admin.getEmail());

        //when
        mockMvc.perform(request(PUT, "/users/password")
               .header("Authorization", token)
               .header("Content-Type", "application/json")
               .content(objectMapper.writeValueAsString(passwordDto)))
               .andExpect(status().isUnauthorized());
    }

    @Test
    @Transactional
    public void changeRole_happyPath()throws Exception{
        //given
        UserCreateDto createDto = dbUserCreator.generateCreateDto();
        UserFullDto createdDto = userService.create(createDto);
        ChangeUserRoleDto changeRoleDto = dbUserCreator.generateRoleDto(createdDto.getId());
        //when
        UserFullDto newAdmin = userService.changeRole(changeRoleDto);
        //then
        Assertions.assertEquals(UserRole.ADMIN, newAdmin.getRole());
    }


}
