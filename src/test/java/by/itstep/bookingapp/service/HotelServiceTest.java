package by.itstep.bookingapp.service;
import by.itstep.bookingapp.BookingappApplicationTests;
import by.itstep.bookingapp.dto.hotel.HotelCreateDto;
import by.itstep.bookingapp.dto.hotel.HotelFullDto;
import by.itstep.bookingapp.dto.hotel.HotelShortDto;
import by.itstep.bookingapp.dto.hotel.HotelUpdateDto;
import by.itstep.bookingapp.dto.room.RoomShortDto;
import by.itstep.bookingapp.entities.CountryEntity;
import by.itstep.bookingapp.entities.HotelEntity;
import by.itstep.bookingapp.entities.RoomEntity;
import by.itstep.bookingapp.repository.BookingRepository;
import by.itstep.bookingapp.repository.CountryRepository;
import by.itstep.bookingapp.repository.HotelRepository;
import by.itstep.bookingapp.repository.RoomRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Commit;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityNotFoundException;
import java.util.List;

public class HotelServiceTest extends BookingappApplicationTests {

    @Autowired
    private HotelRepository hotelRepository;
    @Autowired
    private HotelService hotelService;
    @Autowired
    private RoomRepository roomRepository;
    @Autowired
    private CountryRepository countryRepository;
    @Autowired
    private BookingRepository bookingRepository;

    @Test
    @Transactional
    @Commit
    public void create_happyPath(){
        //given
        HotelCreateDto createDto = generateCreateDto();
        HotelFullDto createdHotelFullDto = hotelService.create(createDto);
        //when
        HotelEntity foundCreatedHotel = hotelRepository.getById(createdHotelFullDto.getId());
        //then
        Assertions.assertNotNull(foundCreatedHotel);
        Assertions.assertEquals(createDto.getName(), foundCreatedHotel.getName());
        Assertions.assertEquals(createDto.getAddress(), foundCreatedHotel.getAddress());
        Assertions.assertEquals(createDto.getCountryId(), foundCreatedHotel.getCountry().getId());
        Assertions.assertEquals(createDto.getPrice(), foundCreatedHotel.getPrice());
        Assertions.assertEquals(createDto.getNumberOfRoomsFor1(), foundCreatedHotel.getNumberOfRoomsFor1());
        Assertions.assertEquals(createDto.getNumberOfRoomsFor2(), foundCreatedHotel.getNumberOfRoomsFor2());
        Assertions.assertEquals(createDto.getNumberOfRoomsFor3(), foundCreatedHotel.getNumberOfRoomsFor3());
        Assertions.assertEquals(createDto.getNumberOfRoomsFor4(), foundCreatedHotel.getNumberOfRoomsFor4());
    }

    @Test
    @Transactional
    public void update_happyPath(){
        //given
        HotelCreateDto createDto = generateCreateDto();
        HotelFullDto createdDto = hotelService.create(createDto);
        HotelUpdateDto updateDto = generateUpdateDto();
        updateDto.setId(createdDto.getId());
        hotelService.update(updateDto);
        //when
        HotelFullDto foundDto = hotelService.findById(createdDto.getId());
        //then
        Assertions.assertNotNull(foundDto);
        Assertions.assertEquals(updateDto.getName(), foundDto.getName());
        Assertions.assertEquals(updateDto.getAddress(), foundDto.getAddress());
        Assertions.assertEquals(updateDto.getCountryId(), foundDto.getCountryId());
        Assertions.assertEquals(updateDto.getPrice(), foundDto.getPrice());
    }

    @Test
    @Transactional
    public void findById_happyPath(){
        //given
        HotelCreateDto createDto = generateCreateDto();
        HotelFullDto createdHotelFullDto = hotelService.create(createDto);
        //when
        HotelFullDto foundByIdDto = hotelService.findById(createdHotelFullDto.getId());
        //then
        Assertions.assertNotNull(foundByIdDto);
        Assertions.assertEquals(createDto.getName(), foundByIdDto.getName());
        Assertions.assertEquals(createDto.getCountryId(), foundByIdDto.getCountryId());
        Assertions.assertEquals(createDto.getPrice(), foundByIdDto.getPrice());
        Assertions.assertEquals(createDto.getAddress(), foundByIdDto.getAddress());
    }

    @Test
    @Transactional
    public void findById_whenNoOneFound(){
        //given
        //when
        //then
        Assertions.assertThrows(EntityNotFoundException.class, ()->hotelService.findById(2));
    }

    @Test
    @Transactional
    public void findAll_happyPath(){
        //given
        HotelCreateDto createDto = generateCreateDto();
        HotelFullDto createdHotelFullDto = hotelService.create(createDto);
        HotelCreateDto createDto2 = generateCreateDto();
        HotelFullDto createdHotelFullDto2 = hotelService.create(createDto2);
        //when
        List <HotelShortDto> dtos = hotelService.findAll();
        //then
        Assertions.assertEquals(2,dtos.size());
    }

    @Test
    @Transactional
    public void deleteHotel_happyPath(){
        //given
        HotelCreateDto createDto = generateCreateDto();
        HotelFullDto createdHotelFullDto = hotelService.create(createDto);
        hotelService.delete(createdHotelFullDto.getId());
        //when
        HotelEntity deletedEntity = hotelRepository.getById(createdHotelFullDto.getId());
        //then
        Assertions.assertNotNull(deletedEntity.getDeletedAt());
    }

    @Test
    @Transactional
    @Commit
    public void deleteRoom_happyPath(){
        //given
        HotelCreateDto createDto = generateCreateDto();
        HotelFullDto createdHotelFullDto = hotelService.create(createDto);
        //when
        HotelFullDto updatedHotel = hotelService.deleteRoom(createdHotelFullDto.getId(),1);
        List<RoomEntity> foundRooms = roomRepository.findByHotelAndGuests(createdHotelFullDto.getId(),1);
        //then
        Assertions.assertEquals(1, foundRooms.size());
        System.out.println("Number of rooms: " + foundRooms.size());
        System.out.println("Hotel id: " + createdHotelFullDto.getId());
    }

    @Test
    @Transactional
    @Commit
    public void addRoom_happyPath(){
        //given
        HotelCreateDto createDto = generateCreateDto();
        HotelFullDto createdHotelFullDto = hotelService.create(createDto);
        //when
        HotelFullDto updatedHotel = hotelService.addRoom(createdHotelFullDto.getId(),2);
        List<RoomEntity> foundRooms = roomRepository.findByHotelAndGuests(createdHotelFullDto.getId(),2);
        //then
        Assertions.assertEquals(2, foundRooms.size());
    }

    @Test
    @Transactional
    public void findAllRooms_happyPath(){
        //given
        HotelCreateDto createDto = generateCreateDto();
        HotelFullDto createdHotelDto = hotelService.create(createDto);
        //when
        List <RoomShortDto> foundDtos  = hotelService.findAllRoomsInHotel(createdHotelDto.getId());
        //then
        Assertions.assertEquals(5,foundDtos.size());
    }


    private HotelCreateDto generateCreateDto(){
        HotelCreateDto hotelCreateDto = new HotelCreateDto();
        hotelCreateDto.setName(FAKER.name().title());
        hotelCreateDto.setAddress(FAKER.address().streetAddress());
        hotelCreateDto.setPrice(5.0);
        hotelCreateDto.setNumberOfRoomsFor1(2);
        hotelCreateDto.setNumberOfRoomsFor2(1);
        hotelCreateDto.setNumberOfRoomsFor3(2);
        hotelCreateDto.setNumberOfRoomsFor4(0);
        CountryEntity country = new CountryEntity();
        country.setName("test country"+Math.random());
        countryRepository.save(country);
        hotelCreateDto.setCountryId(country.getId());
        return hotelCreateDto;
    }

    private HotelUpdateDto generateUpdateDto(){
        HotelUpdateDto hotelUpdateDto = new HotelUpdateDto();
        hotelUpdateDto.setName(FAKER.name().title());
        hotelUpdateDto.setAddress(FAKER.address().streetAddress());
        hotelUpdateDto.setPrice(5.0);
        CountryEntity country = new CountryEntity();
        country.setName("test country"+Math.random());
        countryRepository.save(country);
        hotelUpdateDto.setCountryId(country.getId());
        return hotelUpdateDto;
    }



}
