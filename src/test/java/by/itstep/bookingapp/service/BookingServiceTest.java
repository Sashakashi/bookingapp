package by.itstep.bookingapp.service;
import by.itstep.bookingapp.BookingappApplicationTests;
import by.itstep.bookingapp.dto.booking.BookingCreateDto;
import by.itstep.bookingapp.dto.booking.BookingFullDto;
import by.itstep.bookingapp.dto.user.UserFullDto;
import by.itstep.bookingapp.entities.UserEntity;
import by.itstep.bookingapp.repository.BookingRepository;
import by.itstep.bookingapp.repository.CountryRepository;
import by.itstep.bookingapp.repository.HotelRepository;
import by.itstep.bookingapp.repository.UserRepository;
import by.itstep.bookingapp.utils.DbBookingCreator;
import by.itstep.bookingapp.utils.DbHotelCreator;
import by.itstep.bookingapp.utils.DbUserCreator;
import by.itstep.bookingapp.utils.JwtHelper;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.test.annotation.Commit;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import static org.springframework.http.HttpMethod.DELETE;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.request;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
public class BookingServiceTest extends BookingappApplicationTests {

    @Autowired
    private BookingRepository bookingRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private CountryRepository countryRepository;
    @Autowired
    private HotelRepository hotelRepository;
    @Autowired
    private UserService userService;
    @Autowired
    private HotelService hotelService;
    @Autowired
    private BookingService bookingService;
    @Autowired
    private DbUserCreator dbUserCreator;
    @Autowired
    private DbHotelCreator dbHotelCreator;
    @Autowired
    private DbBookingCreator dbBookingCreator;
    @Autowired
    private JwtHelper jwtHelper;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private MockMvc mockMvc;

    @Test
    @Transactional
    @Commit
    public void create_happyPath()throws Exception{
        //given
        BookingCreateDto dto = dbBookingCreator.generateBookingCreateDto();

        UserFullDto admin = dbUserCreator.addAdminToDb();
        String token = jwtHelper.createToken(admin.getEmail());

        //when
        MvcResult mvcResult = mockMvc.perform(request(POST, "/bookings")
                                     .header("Authorization", token)
                                     .header("Content-Type", "application/json")
                                     .content(objectMapper.writeValueAsString(dto)))
                                     .andExpect(status().isOk())
                                     .andReturn();
        byte [] bytes = mvcResult.getResponse().getContentAsByteArray();
        BookingFullDto foundBooking = objectMapper.readValue(bytes, BookingFullDto.class);
        //then
        Assertions.assertNotNull(foundBooking);
    }

    @Test
    @Transactional
    public void create_whenNoAvailableRoom()throws Exception{
        //given
        BookingCreateDto dto = dbBookingCreator.generateBookingCreateDto();

        UserFullDto admin = dbUserCreator.addAdminToDb();
        String token = jwtHelper.createToken(admin.getEmail());

        mockMvc.perform(request(POST, "/bookings")
               .header("Authorization", token)
               .header("Content-Type", "application/json")
               .content(objectMapper.writeValueAsString(dto)))
               .andExpect(status().isOk());

        BookingCreateDto dto2 = new BookingCreateDto();
        dto2.setUserId(dto.getUserId());
        dto2.setHotelId(dto.getHotelId());
        dto2.setNumberOfPeople(1);
        dto2.setDateIn(Date.valueOf(LocalDate.now().plusDays(4)));
        dto2.setDateOut(Date.valueOf(LocalDate.now().plusDays(8)));
        //when
        mockMvc.perform(request(POST, "/bookings")
               .header("Authorization", token)
               .header("Content-Type", "application/json")
               .content(objectMapper.writeValueAsString(dto2)))
               .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    @Commit
    public void findById_happyPath()throws Exception{
        //given
        BookingCreateDto dto = dbBookingCreator.generateBookingCreateDto();

        UserFullDto admin = dbUserCreator.addAdminToDb();
        String token = jwtHelper.createToken(admin.getEmail());

        MvcResult mvcResult = mockMvc.perform(request(POST, "/bookings")
                                     .header("Authorization", token)
                                     .header("Content-Type", "application/json")
                                     .content(objectMapper.writeValueAsString(dto)))
                                     .andExpect(status().isOk())
                                     .andReturn();
        byte [] bytes = mvcResult.getResponse().getContentAsByteArray();
        //when
        BookingFullDto foundBooking = objectMapper.readValue(bytes, BookingFullDto.class);
        //then
        Assertions.assertNotNull(foundBooking);
    }

    @Test
    @Transactional
    @Commit
    public void findAll_happyPath()throws Exception{
        //given
        BookingCreateDto dto = dbBookingCreator.generateBookingCreateDto();
        BookingCreateDto dto2 = dbBookingCreator.generateBookingCreateDto();

        UserFullDto admin = dbUserCreator.addAdminToDb();
        String token = jwtHelper.createToken(admin.getEmail());

        mockMvc.perform(request(POST, "/bookings")
               .header("Authorization", token)
               .header("Content-Type", "application/json")
               .content(objectMapper.writeValueAsString(dto)))
               .andExpect(status().isOk());
        mockMvc.perform(request(POST, "/bookings")
               .header("Authorization", token)
               .header("Content-Type", "application/json")
               .content(objectMapper.writeValueAsString(dto2)))
               .andExpect(status().isOk());
        //when
        List<BookingFullDto> foundDtos = bookingService.findAll();
        //then
        Assertions.assertEquals(2, foundDtos.size());
    }

    @Test
    @Transactional
    @Commit
    public void deleteAndRefund_happyPath()throws Exception{
        //given
        BookingCreateDto dto = dbBookingCreator.generateBookingCreateDto();

        UserFullDto admin = dbUserCreator.addAdminToDb();
        String token = jwtHelper.createToken(admin.getEmail());

        MvcResult mvcResult = mockMvc.perform(request(POST, "/bookings")
                                     .header("Authorization", token)
                                     .header("Content-Type", "application/json")
                                     .content(objectMapper.writeValueAsString(dto)))
                                     .andExpect(status().isOk())
                                     .andReturn();

        byte [] bytes = mvcResult.getResponse().getContentAsByteArray();
        BookingFullDto foundBooking = objectMapper.readValue(bytes, BookingFullDto.class);
        //when
        mockMvc.perform(request(DELETE, "/bookings/"+foundBooking.getId())
               .header("Authorization", token)
               .header("Content-Type", "application/json"))
               .andExpect(status().isOk());
        //then
        Assertions.assertNotNull(bookingRepository.getById(foundBooking.getId()).getDeletedAt());
        UserEntity user = userRepository.getById( foundBooking.getUserId());
        Assertions.assertEquals(1000, user.getBalance());
    }

}
