package by.itstep.bookingapp;
import by.itstep.bookingapp.utils.DbCleaner;
import com.github.javafaker.Faker;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class BookingappApplicationTests {

	public static final Faker FAKER = new Faker();

	@Autowired
	private DbCleaner dbCleaner;

	@BeforeEach
	public void setUp(){
		dbCleaner.clean();
	}

}
