package by.itstep.bookingapp.controller;
import by.itstep.bookingapp.BookingappApplicationTests;
import by.itstep.bookingapp.dto.booking.BookingCreateDto;
import by.itstep.bookingapp.dto.booking.BookingFullDto;
import by.itstep.bookingapp.dto.user.UserFullDto;
import by.itstep.bookingapp.repository.BookingRepository;
import by.itstep.bookingapp.repository.UserRepository;
import by.itstep.bookingapp.service.BookingService;
import by.itstep.bookingapp.service.UserService;
import by.itstep.bookingapp.utils.DbBookingCreator;
import by.itstep.bookingapp.utils.DbHotelCreator;
import by.itstep.bookingapp.utils.DbUserCreator;
import by.itstep.bookingapp.utils.JwtHelper;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityNotFoundException;
import java.util.List;
import static org.springframework.http.HttpMethod.DELETE;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.request;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
public class BookingControllerTest extends BookingappApplicationTests {

    @Autowired
    private DbUserCreator dbUserCreator;
    @Autowired
    private JwtHelper jwtHelper;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private DbHotelCreator dbHotelCreator;
    @Autowired
    private BookingService bookingService;
    @Autowired
    private DbBookingCreator dbBookingCreator;
    @Autowired
    private BookingController bookingController;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private BookingRepository bookingRepository;
    @Autowired
    private UserService userService;


    @Test
    @Transactional
    public void findById_happyPath()throws Exception {
        //given
        BookingCreateDto createDto = dbBookingCreator.generateBookingCreateDto();

        UserFullDto admin = dbUserCreator.addAdminToDb();
        String token = jwtHelper.createToken(admin.getEmail());
        //when
        MvcResult mvcResult = mockMvc.perform(request(POST, "/bookings")
                                     .header("Authorization", token)
                                     .header("Content-Type", "application/json")
                                     .content(objectMapper.writeValueAsString(createDto)))
                                     .andExpect(status().isOk())
                                     .andReturn();
        byte [] bytes = mvcResult.getResponse().getContentAsByteArray();
        BookingFullDto foundBooking = objectMapper.readValue(bytes, BookingFullDto.class);
        //then
        Assertions.assertNotNull(foundBooking);
        Assertions.assertEquals(foundBooking.getUserId(), createDto.getUserId());
    }

    @Test
    public void findById_whenNotFound()throws Exception{
        //given
        //when
        //then
        Assertions.assertThrows(EntityNotFoundException.class, ()->bookingController.findById(1));
    }

    @Test
    @Transactional
    public void findAll_happyPath()throws Exception {
        //given
        BookingCreateDto dto = dbBookingCreator.generateBookingCreateDto();
        BookingCreateDto dto2 = dbBookingCreator.generateBookingCreateDto();
        UserFullDto admin = dbUserCreator.addAdminToDb();
        String token = jwtHelper.createToken(admin.getEmail());
        //when
        mockMvc.perform(request(POST, "/bookings")
               .header("Authorization", token)
               .header("Content-Type", "application/json")
               .content(objectMapper.writeValueAsString(dto)))
               .andExpect(status().isOk())
               .andReturn();
        mockMvc.perform(request(POST, "/bookings")
               .header("Authorization", token)
               .header("Content-Type", "application/json")
               .content(objectMapper.writeValueAsString(dto2)))
               .andExpect(status().isOk())
               .andReturn();
        List<BookingFullDto> foundBookings = bookingService.findAll();
        //then
        Assertions.assertEquals(2, foundBookings.size());
    }

    @Test
    public void findAll_whenEmpty()throws Exception {
        //given
        //when
        List<BookingFullDto> foundBookings = bookingService.findAll();
        //then
        Assertions.assertEquals(0, foundBookings.size());
    }

    @Test
    @Transactional
    public void createAndPay_happyPath_user()throws Exception {
        //given
        BookingCreateDto createDto = dbBookingCreator.generateBookingCreateDto();

        UserFullDto admin = dbUserCreator.addAdminToDb();
        String token = jwtHelper.createToken(userRepository.getById(createDto.getUserId()).getEmail());
        //when
        MvcResult mvcResult = mockMvc.perform(request(POST, "/bookings")
                                     .header("Authorization", token)
                                     .header("Content-Type", "application/json")
                                     .content(objectMapper.writeValueAsString(createDto)))
                                     .andExpect(status().isOk())
                                     .andReturn();
        byte [] bytes = mvcResult.getResponse().getContentAsByteArray();
        BookingFullDto foundBooking = objectMapper.readValue(bytes, BookingFullDto.class);
        //then
        Assertions.assertNotNull(foundBooking);
        Assertions.assertEquals(foundBooking.getUserId(), createDto.getUserId());
    }

    @Test
    @Transactional
    public void createAndPay_userWithNoMoney()throws Exception {
        //given
        BookingCreateDto createDto = dbBookingCreator.generateBookingCreateDtoWithNoMoney();

        UserFullDto admin = dbUserCreator.addAdminToDb();
        String token = jwtHelper.createToken(userRepository.getById(createDto.getUserId()).getEmail());
        //when
        mockMvc.perform(request(POST, "/bookings")
               .header("Authorization", token)
               .header("Content-Type", "application/json")
               .content(objectMapper.writeValueAsString(createDto)))
               .andExpect(status().isMethodNotAllowed());
    }

    @Test
    @Transactional
    public void createAndPay_whenNotAuthenticated()throws Exception {
        //given
        BookingCreateDto createDto = dbBookingCreator.generateBookingCreateDto();

        //when
        mockMvc.perform(request(POST, "/bookings")
               .header("Content-Type", "application/json")
               .content(objectMapper.writeValueAsString(createDto)))
               .andExpect(status().isForbidden());
    }

    @Test
    @Transactional
    public void refund_happyPath_admin()throws Exception {
        //given
        BookingCreateDto createDto = dbBookingCreator.generateBookingCreateDto();

        UserFullDto admin = dbUserCreator.addAdminToDb();
        String token = jwtHelper.createToken(admin.getEmail());

        MvcResult mvcResult = mockMvc.perform(request(POST, "/bookings")
                                     .header("Authorization", token)
                                     .header("Content-Type", "application/json")
                                     .content(objectMapper.writeValueAsString(createDto)))
                                     .andExpect(status().isOk())
                                     .andReturn();
        byte [] bytes = mvcResult.getResponse().getContentAsByteArray();
        BookingFullDto foundBooking = objectMapper.readValue(bytes, BookingFullDto.class);

        //when
        MvcResult mvcRefundResult = mockMvc.perform(request(DELETE, "/bookings/"+foundBooking.getId())
                                           .header("Authorization", token)
                                           .header("Content-Type", "application/json"))
                                           .andExpect(status().isOk())
                                           .andReturn();
        byte [] bytes2 = mvcResult.getResponse().getContentAsByteArray();
        BookingFullDto deletedBooking = objectMapper.readValue(bytes, BookingFullDto.class);
        //then
        Assertions.assertNotNull(bookingRepository.getById(deletedBooking.getId()).getDeletedAt());
    }

    @Test
    @Transactional
    public void refund_happyPath_user()throws Exception {
        //given
        BookingCreateDto createDto = dbBookingCreator.generateBookingCreateDto();

        String token = jwtHelper.createToken(userRepository.getById(createDto.getUserId()).getEmail());

        MvcResult mvcResult = mockMvc.perform(request(POST, "/bookings")
                                     .header("Authorization", token)
                                     .header("Content-Type", "application/json")
                                     .content(objectMapper.writeValueAsString(createDto)))
                                     .andExpect(status().isOk())
                                     .andReturn();
        byte [] bytes = mvcResult.getResponse().getContentAsByteArray();
        BookingFullDto foundBooking = objectMapper.readValue(bytes, BookingFullDto.class);

        //when
        MvcResult mvcRefundResult = mockMvc.perform(request(DELETE, "/bookings/"+foundBooking.getId())
                                           .header("Authorization", token)
                                           .header("Content-Type", "application/json"))
                                           .andExpect(status().isOk())
                                           .andReturn();
        byte [] bytes2 = mvcResult.getResponse().getContentAsByteArray();
        BookingFullDto deletedBooking = objectMapper.readValue(bytes, BookingFullDto.class);
        //then
        Assertions.assertNotNull(bookingRepository.getById(deletedBooking.getId()).getDeletedAt());
    }

    @Test
    @Transactional
    public void refund_whenAnotherUser()throws Exception {
        //given
        BookingCreateDto createDto = dbBookingCreator.generateBookingCreateDto();

        String token = jwtHelper.createToken(userRepository.getById(createDto.getUserId()).getEmail());

        MvcResult mvcResult = mockMvc.perform(request(POST, "/bookings")
                                     .header("Authorization", token)
                                     .header("Content-Type", "application/json")
                                     .content(objectMapper.writeValueAsString(createDto)))
                                     .andExpect(status().isOk())
                                     .andReturn();
        byte [] bytes = mvcResult.getResponse().getContentAsByteArray();
        BookingFullDto foundBooking = objectMapper.readValue(bytes, BookingFullDto.class);

        //when
        UserFullDto anotherUser = userService.create(dbUserCreator.generateCreateDto());
        String anotherUserToken = jwtHelper.createToken(anotherUser.getEmail());

        mockMvc.perform(request(DELETE, "/bookings/"+foundBooking.getId())
               .header("Authorization", anotherUserToken)
               .header("Content-Type", "application/json"))
               .andExpect(status().isMethodNotAllowed());
    }

    @Test
    @Transactional
    public void refund_whenNotAuthenticated()throws Exception {
        //given
        BookingCreateDto createDto = dbBookingCreator.generateBookingCreateDto();

        String token = jwtHelper.createToken(userRepository.getById(createDto.getUserId()).getEmail());

        MvcResult mvcResult = mockMvc.perform(request(POST, "/bookings")
                                     .header("Authorization", token)
                                     .header("Content-Type", "application/json")
                                     .content(objectMapper.writeValueAsString(createDto)))
                                     .andExpect(status().isOk())
                                     .andReturn();
        byte [] bytes = mvcResult.getResponse().getContentAsByteArray();
        BookingFullDto foundBooking = objectMapper.readValue(bytes, BookingFullDto.class);

        //when
        mockMvc.perform(request(DELETE, "/bookings/"+foundBooking.getId())
               .header("Content-Type", "application/json"))
               .andExpect(status().isForbidden());
    }
}
