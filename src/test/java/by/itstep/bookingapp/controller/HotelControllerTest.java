package by.itstep.bookingapp.controller;
import by.itstep.bookingapp.BookingappApplicationTests;
import by.itstep.bookingapp.dto.hotel.HotelCreateDto;
import by.itstep.bookingapp.dto.hotel.HotelFullDto;
import by.itstep.bookingapp.dto.hotel.HotelShortDto;
import by.itstep.bookingapp.dto.hotel.HotelUpdateDto;
import by.itstep.bookingapp.dto.room.RoomShortDto;
import by.itstep.bookingapp.dto.user.UserCreateDto;
import by.itstep.bookingapp.dto.user.UserFullDto;
import by.itstep.bookingapp.repository.HotelRepository;
import by.itstep.bookingapp.repository.RoomRepository;
import by.itstep.bookingapp.service.HotelService;
import by.itstep.bookingapp.service.UserService;
import by.itstep.bookingapp.utils.DbHotelCreator;
import by.itstep.bookingapp.utils.DbUserCreator;
import by.itstep.bookingapp.utils.JwtHelper;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityNotFoundException;
import java.util.List;
import static org.springframework.http.HttpMethod.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.request;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
public class HotelControllerTest extends BookingappApplicationTests {

    @Autowired
    private DbHotelCreator dbHotelCreator;
    @Autowired
    private HotelService hotelService;
    @Autowired
    private UserService userService;
    @Autowired
    private RoomRepository roomRepository;
    @Autowired
    private  HotelController hotelController;
    @Autowired
    private HotelRepository hotelRepository;
    @Autowired
    private DbUserCreator dbUserCreator;
    @Autowired
    private JwtHelper jwtHelper;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private MockMvc mockMvc;


    @Test
    public void findById_happyPath()throws Exception{
        //given
        HotelCreateDto createDto = dbHotelCreator.generateCreateDto();
        HotelFullDto createdHotel = hotelService.create(createDto);
        //when
        HotelFullDto foundDto = hotelController.findById(createdHotel.getId());
        //then
        Assertions.assertEquals(createdHotel.getId(), foundDto.getId());
    }

    @Test
    public void findById_whenNotFound()throws Exception{
        //given
        //when
        //then
        Assertions.assertThrows(EntityNotFoundException.class, ()->hotelController.findById(1));
    }

    @Test
    public void findAll_happyPath()throws Exception {
        //given
        HotelCreateDto createDto = dbHotelCreator.generateCreateDto();
        hotelService.create(createDto);
        HotelCreateDto createDto2 = dbHotelCreator.generateCreateDto();
        hotelService.create(createDto2);
        //when
        List<HotelShortDto> foundDtos = hotelService.findAll();
        //then
        Assertions.assertEquals(2, foundDtos.size());
    }

    @Test
    public void findAll_whenEmpty()throws Exception {
        //given
        //when
        List<HotelShortDto> foundDtos = hotelService.findAll();
        //then
        Assertions.assertEquals(0, foundDtos.size());
    }

    @Test
    @Transactional
    public void create_happyPath()throws Exception{
        //given
        HotelCreateDto createDto = dbHotelCreator.generateCreateDto();
        UserFullDto admin = dbUserCreator.addAdminToDb();
        String token = jwtHelper.createToken(admin.getEmail());
        //when
        MvcResult mvcResult = mockMvc.perform(request(POST, "/hotels")
                                     .header("Authorization", token)
                                     .header("Content-Type", "application/json")
                                     .content(objectMapper.writeValueAsString(createDto)))
                                     .andExpect(status().isOk())
                                     .andReturn();

        byte [] bytes = mvcResult.getResponse().getContentAsByteArray();
        HotelFullDto foundHotel = objectMapper.readValue(bytes, HotelFullDto.class);
        //then
        Assertions.assertNotNull(foundHotel.getId());
        Assertions.assertEquals(createDto.getName(), foundHotel.getName());
        Assertions.assertEquals(createDto.getAddress(), foundHotel.getAddress());
        Assertions.assertEquals(createDto.getPrice(), foundHotel.getPrice());
        Assertions.assertEquals(createDto.getCountryId(), foundHotel.getCountryId());
        Assertions.assertEquals(createDto.getNumberOfRoomsFor2(), foundHotel.getNumberOfRoomsFor2());
    }

    @Test
    public void create_whenNotAdmin()throws Exception{
        //given
        HotelCreateDto createDto = dbHotelCreator.generateCreateDto();
        UserCreateDto user = dbUserCreator.generateCreateDto();
        userService.create(user);
        String token = jwtHelper.createToken(user.getEmail());
        //when
        mockMvc.perform(request(POST, "/hotels")
               .header("Authorization", token)
               .header("Content-Type", "application/json")
               .content(objectMapper.writeValueAsString(createDto)))
               .andExpect(status().isForbidden());
    }

    @Test
    public void create_whenNotAuthenticated()throws Exception{
        //given
        HotelCreateDto createDto = dbHotelCreator.generateCreateDto();
        //when
        mockMvc.perform(request(POST, "/hotels")
               .header("Content-Type", "application/json")
               .content(objectMapper.writeValueAsString(createDto)))
               .andExpect(status().isForbidden());
    }

    @Test
    @Transactional
    public void update_happyPath()throws Exception{
        //given
        HotelCreateDto createDto = dbHotelCreator.generateCreateDto();
        HotelFullDto createdHotel = hotelService.create(createDto);
        HotelUpdateDto updateDto = dbHotelCreator.generateUpdateDto(createdHotel.getId(), createdHotel.getCountryId());
        UserFullDto admin = dbUserCreator.addAdminToDb();
        String token = jwtHelper.createToken(admin.getEmail());
        //when
        MvcResult mvcResult = mockMvc.perform(request(PUT, "/hotels")
                                     .header("Authorization", token)
                                     .header("Content-Type", "application/json")
                                     .content(objectMapper.writeValueAsString(updateDto)))
                                     .andExpect(status().isOk())
                                     .andReturn();

        byte [] bytes = mvcResult.getResponse().getContentAsByteArray();
        HotelFullDto foundHotel = objectMapper.readValue(bytes, HotelFullDto.class);
        //then
        Assertions.assertNotNull(foundHotel.getId());
        Assertions.assertEquals(updateDto.getName(), foundHotel.getName());
        Assertions.assertEquals(updateDto.getAddress(), foundHotel.getAddress());
        Assertions.assertEquals(updateDto.getPrice(), foundHotel.getPrice());
        Assertions.assertEquals(updateDto.getCountryId(), foundHotel.getCountryId());
    }

    @Test
    public void update_whenNotAdmin()throws Exception{
        //given
        HotelCreateDto createDto = dbHotelCreator.generateCreateDto();
        HotelFullDto createdHotel = hotelService.create(createDto);
        HotelUpdateDto updateDto = dbHotelCreator.generateUpdateDto(createdHotel.getId(), createdHotel.getCountryId());
        UserCreateDto user = dbUserCreator.generateCreateDto();
        userService.create(user);
        String token = jwtHelper.createToken(user.getEmail());
        //when
        mockMvc.perform(request(PUT, "/hotels")
               .header("Authorization", token)
               .header("Content-Type", "application/json")
               .content(objectMapper.writeValueAsString(updateDto)))
               .andExpect(status().isForbidden());
    }

    @Test
    public void update_whenNotAuthenticated()throws Exception{
        //given
        HotelCreateDto createDto = dbHotelCreator.generateCreateDto();
        HotelFullDto createdHotel = hotelService.create(createDto);
        HotelUpdateDto updateDto = dbHotelCreator.generateUpdateDto(createdHotel.getId(), createdHotel.getCountryId());
        //when
        mockMvc.perform(request(PUT, "/hotels")
               .header("Content-Type", "application/json")
               .content(objectMapper.writeValueAsString(updateDto)))
               .andExpect(status().isForbidden());
    }

    @Test
    @Transactional
    public void delete_happyPath()throws Exception{
        //given
        HotelCreateDto createDto = dbHotelCreator.generateCreateDto();
        HotelFullDto createdHotel = hotelService.create(createDto);
        UserFullDto admin = dbUserCreator.addAdminToDb();
        String token = jwtHelper.createToken(admin.getEmail());
        //when
        mockMvc.perform(request(DELETE, "/hotels/"+createdHotel.getId())
               .header("Authorization", token))
               .andExpect(status().isOk());
        //then
        Assertions.assertNotNull(hotelRepository.getById(createdHotel.getId()).getDeletedAt());
    }

    @Test
    public void delete_whenNotAdmin()throws Exception{
        //given
        HotelCreateDto createDto = dbHotelCreator.generateCreateDto();
        HotelFullDto createdHotel = hotelService.create(createDto);
        UserCreateDto user = dbUserCreator.generateCreateDto();
        userService.create(user);
        String token = jwtHelper.createToken(user.getEmail());
        //when
        mockMvc.perform(request(DELETE, "/hotels/"+createdHotel.getId())
               .header("Authorization", token))
               .andExpect(status().isForbidden());
    }

    @Test
    public void delete_whenNotAuthenticated()throws Exception{
        //given
        HotelCreateDto createDto = dbHotelCreator.generateCreateDto();
        HotelFullDto createdHotel = hotelService.create(createDto);
        //when
        mockMvc.perform(request(DELETE, "/hotels/"+createdHotel.getId()))
               .andExpect(status().isForbidden());
    }

    @Test
    public void findAllRooms_happyPath()throws Exception {
        //given
        HotelCreateDto createDto = dbHotelCreator.generateCreateDto();
        HotelFullDto createdHotel = hotelService.create(createDto);
        //when
        List<RoomShortDto> foundRooms = hotelController.findAllRooms(createdHotel.getId());
        //then
        Assertions.assertEquals(7, foundRooms.size());
    }

    @Test
    @Transactional
    public void deleteRoom_happyPath()throws Exception{
        //given
        HotelCreateDto createDto = dbHotelCreator.generateCreateDto();
        HotelFullDto createdHotel = hotelService.create(createDto);
        UserFullDto admin = dbUserCreator.addAdminToDb();
        String token = jwtHelper.createToken(admin.getEmail());
        //when
        mockMvc.perform(request(DELETE, "/hotels/"+createdHotel.getId()+"/delete/"+2)
               .header("Authorization", token))
               .andExpect(status().isOk());
        List<RoomShortDto> foundRooms = hotelController.findAllRooms(createdHotel.getId());
        //then
        Assertions.assertEquals(6, foundRooms.size());
    }

    @Test
    public void deleteRoom_whenNotAdmin()throws Exception{
        //given
        HotelCreateDto createDto = dbHotelCreator.generateCreateDto();
        HotelFullDto createdHotel = hotelService.create(createDto);
        UserCreateDto user = dbUserCreator.generateCreateDto();
        userService.create(user);
        String token = jwtHelper.createToken(user.getEmail());
        //when
        mockMvc.perform(request(DELETE, "/hotels/"+createdHotel.getId()+"/delete/"+2)
               .header("Authorization", token))
               .andExpect(status().isForbidden());
    }

    @Test
    public void deleteRoom_whenNotAuthenticated()throws Exception{
        //given
        HotelCreateDto createDto = dbHotelCreator.generateCreateDto();
        HotelFullDto createdHotel = hotelService.create(createDto);
        //when
        mockMvc.perform(request(DELETE, "/hotels/"+createdHotel.getId()+"/delete/"+2))
               .andExpect(status().isForbidden());
    }

    @Test
    @Transactional
    public void addRoom_happyPath()throws Exception{
        //given
        HotelCreateDto createDto = dbHotelCreator.generateCreateDto();
        HotelFullDto createdHotel = hotelService.create(createDto);
        UserFullDto admin = dbUserCreator.addAdminToDb();
        String token = jwtHelper.createToken(admin.getEmail());
        //when
        mockMvc.perform(request(DELETE, "/hotels/"+createdHotel.getId()+"/add/"+3)
               .header("Authorization", token))
               .andExpect(status().isOk());
        List<RoomShortDto> foundRooms = hotelController.findAllRooms(createdHotel.getId());
        //then
        Assertions.assertEquals(8, foundRooms.size());
    }

    @Test
    public void addRoom_whenNotAdmin()throws Exception{
        //given
        HotelCreateDto createDto = dbHotelCreator.generateCreateDto();
        HotelFullDto createdHotel = hotelService.create(createDto);
        UserCreateDto user = dbUserCreator.generateCreateDto();
        userService.create(user);
        String token = jwtHelper.createToken(user.getEmail());
        //when
        mockMvc.perform(request(DELETE, "/hotels/"+createdHotel.getId()+"/add/"+2)
               .header("Authorization", token))
               .andExpect(status().isForbidden());
    }

    @Test
    public void addRoom_whenNotAuthenticated()throws Exception{
        //given
        HotelCreateDto createDto = dbHotelCreator.generateCreateDto();
        HotelFullDto createdHotel = hotelService.create(createDto);
        //when
        mockMvc.perform(request(DELETE, "/hotels/"+createdHotel.getId()+"/add/"+2))
               .andExpect(status().isForbidden());
    }


}
