package by.itstep.bookingapp.controller;
import by.itstep.bookingapp.BookingappApplicationTests;
import by.itstep.bookingapp.dto.country.CountryCreateDto;
import by.itstep.bookingapp.dto.country.CountryFullDto;
import by.itstep.bookingapp.dto.user.UserCreateDto;
import by.itstep.bookingapp.dto.user.UserFullDto;
import by.itstep.bookingapp.repository.CountryRepository;
import by.itstep.bookingapp.service.CountryService;
import by.itstep.bookingapp.service.UserService;
import by.itstep.bookingapp.utils.DbUserCreator;
import by.itstep.bookingapp.utils.JwtHelper;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityNotFoundException;
import java.util.List;
import static org.springframework.http.HttpMethod.DELETE;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.request;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
public class CountryControllerTest extends BookingappApplicationTests {

    @Autowired
    private CountryService countryService;
    @Autowired
    private CountryRepository countryRepository;
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private DbUserCreator dbUserCreator;
    @Autowired
    private UserService userService;
    @Autowired
    private JwtHelper jwtHelper;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private CountryController countryController;


    @Test
    public void findById_happyPath()throws Exception{
        //given
        CountryCreateDto createDto = new CountryCreateDto();
        createDto.setName("test country" + Math.random());
        CountryFullDto createdCountry = countryService.create(createDto);
        //when
        CountryFullDto foundDto = countryController.findById(createdCountry.getId());
        //then
        Assertions.assertEquals(createdCountry.getId(), foundDto.getId());
    }

    @Test
    public void findById_whenNotFound()throws Exception{
        //given
        //when
        //then
        Assertions.assertThrows(EntityNotFoundException.class, ()->countryController.findById(1));
    }

    @Test
    public void findAll_happyPath()throws Exception{
        //given
        CountryCreateDto createDto = new CountryCreateDto();
        createDto.setName("test country" + Math.random());
        countryService.create(createDto);
        CountryCreateDto createDto2 = new CountryCreateDto();
        createDto2.setName("test country" + Math.random());
        countryService.create(createDto2);
        //when
        List<CountryFullDto> foundDtos = countryController.findAll();
        //then
        Assertions.assertEquals(2, foundDtos.size());
    }

    @Test
    public void findAll_whenEmpty()throws Exception{
        //given
        //when
        List<CountryFullDto> foundDtos = countryController.findAll();
        //then
        Assertions.assertEquals(0, foundDtos.size());
    }

    @Test
    @Transactional
    public void create_happyPath()throws Exception{
        //given
        CountryCreateDto createDto = new CountryCreateDto();
        createDto.setName("test country" + Math.random());
        UserFullDto admin = dbUserCreator.addAdminToDb();
        String token = jwtHelper.createToken(admin.getEmail());
        //when
        MvcResult mvcResult = mockMvc.perform(request(POST, "/countries")
                                     .header("Authorization", token)
                                     .header("Content-Type", "application/json")
                                     .content(objectMapper.writeValueAsString(createDto)))
                                     .andExpect(status().isOk())
                                     .andReturn();

        byte [] bytes = mvcResult.getResponse().getContentAsByteArray();
        CountryFullDto foundCountry = objectMapper.readValue(bytes, CountryFullDto.class);
        //then
        Assertions.assertNotNull(foundCountry.getId());
        Assertions.assertEquals(createDto.getName(), foundCountry.getName());
    }

    @Test
    public void create_whenNotAdmin()throws Exception{
        //given
        CountryCreateDto createDto = new CountryCreateDto();
        createDto.setName("test country" + Math.random());
        UserCreateDto user = dbUserCreator.generateCreateDto();
        userService.create(user);
        String token = jwtHelper.createToken(user.getEmail());
        //when
        mockMvc.perform(request(POST, "/countries")
               .header("Authorization", token)
               .header("Content-Type", "application/json")
               .content(objectMapper.writeValueAsString(createDto)))
               .andExpect(status().isForbidden());
    }

    @Test
    public void create_whenNotAuthenticated()throws Exception{
        //given
        CountryCreateDto createDto = new CountryCreateDto();
        createDto.setName("test country" + Math.random());
        //when
        mockMvc.perform(request(POST, "/countries")
               .header("Content-Type", "application/json")
               .content(objectMapper.writeValueAsString(createDto)))
               .andExpect(status().isForbidden());
    }

    @Test
    @Transactional
    public void delete_happyPath()throws Exception{
        //given
        CountryCreateDto createDto = new CountryCreateDto();
        createDto.setName("test country" + Math.random());
        CountryFullDto country = countryService.create(createDto);
        UserFullDto admin = dbUserCreator.addAdminToDb();
        String token = jwtHelper.createToken(admin.getEmail());
        //when
       mockMvc.perform(request(DELETE, "/countries/"+country.getId())
              .header("Authorization", token))
              .andExpect(status().isOk());
        //then
        Assertions.assertNotNull(countryRepository.getById(country.getId()).getDeletedAt());
    }

    @Test
    public void delete_whenNotAdmin()throws Exception{
        //given
        CountryCreateDto createDto = new CountryCreateDto();
        createDto.setName("test country" + Math.random());
        CountryFullDto country = countryService.create(createDto);
        UserCreateDto user = dbUserCreator.generateCreateDto();
        userService.create(user);
        String token = jwtHelper.createToken(user.getEmail());
        //when
        mockMvc.perform(request(DELETE, "/countries/"+country.getId())
               .header("Authorization", token))
               .andExpect(status().isForbidden());
    }

    @Test
    public void delete_whenNotAuthenticated()throws Exception{
        //given
        CountryCreateDto createDto = new CountryCreateDto();
        createDto.setName("test country" + Math.random());
        CountryFullDto country = countryService.create(createDto);
        //when
        mockMvc.perform(request(DELETE, "/countries/"+country.getId()))
               .andExpect(status().isForbidden());
    }

}
