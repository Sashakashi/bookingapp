package by.itstep.bookingapp.controller;
import by.itstep.bookingapp.BookingappApplicationTests;
import by.itstep.bookingapp.dto.user.*;
import by.itstep.bookingapp.entities.UserEntity;
import by.itstep.bookingapp.entities.UserRole;
import by.itstep.bookingapp.repository.UserRepository;
import by.itstep.bookingapp.service.UserService;
import by.itstep.bookingapp.utils.DbUserCreator;
import by.itstep.bookingapp.utils.JwtHelper;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityNotFoundException;
import java.util.List;
import static org.springframework.http.HttpMethod.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.request;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
public class UserControllerTest extends BookingappApplicationTests {

    @Autowired
    private DbUserCreator dbUserCreator;
    @Autowired
    private UserService userService;
    @Autowired
    private UserController userController;
    @Autowired
    private JwtHelper jwtHelper;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Test
    @Transactional
    public void findById_happyPath()throws Exception{
        //given
        UserCreateDto createDto = dbUserCreator.generateCreateDto();
        UserFullDto createdUser = userService.create(createDto);
        //when
        UserFullDto foundDto = userController.findById(createdUser.getId());
        UserEntity foundEntity = userRepository.getById(foundDto.getId());
        //then
        Assertions.assertEquals(createdUser.getId(), foundDto.getId());
        if(passwordEncoder.matches(createDto.getPassword()+createDto.getEmail(), foundEntity.getPassword())){
            Assertions.assertTrue(1==1);
        }else {
            Assertions.assertTrue(1==2);
        }
    }

    @Test
    public void findById_whenNotFound()throws Exception{
        //given
        //when
        //then
        Assertions.assertThrows(EntityNotFoundException.class, ()->userController.findById(1));
    }

    @Test
    public void findAll_happyPath()throws Exception{
        //given
        UserCreateDto createDto = dbUserCreator.generateCreateDto();
        userService.create(createDto);
        UserCreateDto createDto2 = dbUserCreator.generateCreateDto();
        userService.create(createDto2);
        UserCreateDto createDto3 = dbUserCreator.generateCreateDto();
        userService.create(createDto3);
        //when
        List<UserShortDto> foundDtos = userController.findAll();
        //then
        Assertions.assertEquals(3, foundDtos.size());
    }

    @Test
    public void findAll_whenEmpty()throws Exception{
        //given
        //when
        List<UserShortDto> foundDtos = userController.findAll();
        //then
        Assertions.assertEquals(0, foundDtos.size());
    }

    @Test
    @Transactional
    public void create_happyPath()throws Exception{
        //given
        UserCreateDto createDto = dbUserCreator.generateCreateDto();
        //when
        MvcResult mvcResult = mockMvc.perform(request(POST, "/users")
                                     .header("Content-Type", "application/json")
                                     .content(objectMapper.writeValueAsString(createDto)))
                                     .andExpect(status().isOk())
                                     .andReturn();
        byte [] bytes = mvcResult.getResponse().getContentAsByteArray();
        UserFullDto foundUser = objectMapper.readValue(bytes, UserFullDto.class);
        UserEntity foundEntity = userRepository.getById(foundUser.getId());
        //then
        Assertions.assertNotNull(foundUser.getId());
        Assertions.assertEquals(createDto.getName(), foundUser.getName());
        Assertions.assertEquals(createDto.getLastname(), foundUser.getLastname());
        Assertions.assertEquals(createDto.getPhone(), foundUser.getPhone());
        Assertions.assertEquals(createDto.getEmail(), foundUser.getEmail());
        if(passwordEncoder.matches(createDto.getPassword()+createDto.getEmail(), foundEntity.getPassword())){
            Assertions.assertTrue(1==1);
        }else {
            Assertions.assertTrue(1==2);
        }
    }

    @Test
    @Transactional
    public void update_happyPath_currentUser()throws Exception{
        //given
        UserCreateDto createDto = dbUserCreator.generateCreateDto();
        UserFullDto createdUser = userService.create(createDto);
        UserUpdateDto updateDto = dbUserCreator.generateUpdateDto(createdUser.getId());
        String token = jwtHelper.createToken(createDto.getEmail());
        //when
        MvcResult mvcResult = mockMvc.perform(request(PUT, "/users")
                                     .header("Authorization", token)
                                     .header("Content-Type", "application/json")
                                     .content(objectMapper.writeValueAsString(updateDto)))
                                     .andExpect(status().isOk())
                                     .andReturn();
        byte [] bytes = mvcResult.getResponse().getContentAsByteArray();
        UserFullDto foundUser = objectMapper.readValue(bytes, UserFullDto.class);
        UserEntity foundEntity = userRepository.getById(foundUser.getId());
        //then
        Assertions.assertNotNull(foundUser.getId());
        Assertions.assertEquals(updateDto.getName(), foundUser.getName());
        Assertions.assertEquals(updateDto.getLastname(), foundUser.getLastname());
        Assertions.assertEquals(updateDto.getPhone(), foundUser.getPhone());
        Assertions.assertEquals(updateDto.getId(), foundUser.getId());
        if(passwordEncoder.matches(createDto.getPassword()+createDto.getEmail(), foundEntity.getPassword())){
            Assertions.assertTrue(1==1);
        }else {
            Assertions.assertTrue(1==2);
        }
    }

    @Test
    public void update_whenAnotherUser()throws Exception{
        //given
        UserCreateDto createDto = dbUserCreator.generateCreateDto();
        UserFullDto createdUser = userService.create(createDto);
        UserUpdateDto updateDto = dbUserCreator.generateUpdateDto(createdUser.getId());

        UserCreateDto createAnotherUser = dbUserCreator.generateCreateDto();
        UserFullDto createdAnotherUser = userService.create(createAnotherUser);
        String token = jwtHelper.createToken(createdAnotherUser.getEmail());

        //when
        mockMvc.perform(request(PUT, "/users")
               .header("Authorization", token)
               .header("Content-Type", "application/json")
               .content(objectMapper.writeValueAsString(updateDto)))
               .andExpect(status().isMethodNotAllowed());
    }

    @Test
    @Transactional
    public void update_happyPath_admin()throws Exception{
        //given
        UserCreateDto createDto = dbUserCreator.generateCreateDto();
        UserFullDto createdUser = userService.create(createDto);
        UserUpdateDto updateDto = dbUserCreator.generateUpdateDto(createdUser.getId());
        UserFullDto admin = dbUserCreator.addAdminToDb();
        String token = jwtHelper.createToken(admin.getEmail());
        //when
        MvcResult mvcResult = mockMvc.perform(request(PUT, "/users")
                                     .header("Authorization", token)
                                     .header("Content-Type", "application/json")
                                     .content(objectMapper.writeValueAsString(updateDto)))
                                     .andExpect(status().isOk())
                                     .andReturn();
        byte [] bytes = mvcResult.getResponse().getContentAsByteArray();
        UserFullDto foundUser = objectMapper.readValue(bytes, UserFullDto.class);
        UserEntity foundEntity = userRepository.getById(foundUser.getId());
        //then
        Assertions.assertNotNull(foundUser.getId());
        Assertions.assertEquals(updateDto.getName(), foundUser.getName());
        Assertions.assertEquals(updateDto.getLastname(), foundUser.getLastname());
        Assertions.assertEquals(updateDto.getPhone(), foundUser.getPhone());
        Assertions.assertEquals(updateDto.getId(), foundUser.getId());
        if(passwordEncoder.matches(createDto.getPassword()+createDto.getEmail(), foundEntity.getPassword())){
            Assertions.assertTrue(1==1);
        }else {
            Assertions.assertTrue(1==2);
        }
    }

    @Test
    public void update_whenNotAuthenticated()throws Exception{
        //given
        UserCreateDto createDto = dbUserCreator.generateCreateDto();
        UserFullDto createdUser = userService.create(createDto);
        UserUpdateDto updateDto = dbUserCreator.generateUpdateDto(createdUser.getId());
        //when
        mockMvc.perform(request(PUT, "/users")
               .header("Content-Type", "application/json")
               .content(objectMapper.writeValueAsString(updateDto)))
               .andExpect(status().isForbidden());
    }

    @Test
    @Transactional
    public void changePassword_happyPath_admin()throws Exception{
        //given
        UserCreateDto createDto = dbUserCreator.generateCreateDto();
        UserFullDto createdUser = userService.create(createDto);
        ChangeUserPasswordDto passwordDto = dbUserCreator.generateChangePasswordDto(createdUser.getId(),
                                                                                     createDto.getPassword());
        UserFullDto admin = dbUserCreator.addAdminToDb();
        String token = jwtHelper.createToken(admin.getEmail());
        //when
        mockMvc.perform(request(PUT, "/users/password")
               .header("Authorization", token)
               .header("Content-Type", "application/json")
               .content(objectMapper.writeValueAsString(passwordDto)))
               .andExpect(status().isOk());
        UserFullDto foundUser = userService.findById(createdUser.getId());
        UserEntity foundEntity = userRepository.getById(foundUser.getId());
        //then
        Assertions.assertNotNull(foundUser.getId());
        Assertions.assertEquals(createdUser.getLastname(), foundUser.getLastname());
        Assertions.assertEquals(createdUser.getPhone(), foundUser.getPhone());
        if(passwordEncoder.matches(passwordDto.getNewPassword()+createDto.getEmail(), foundEntity.getPassword())){
            Assertions.assertTrue(1==1);
        }else {
            Assertions.assertTrue(1==2);
        }
    }

    @Test
    @Transactional
    public void changePassword_happyPath_user()throws Exception{
        //given
        UserCreateDto createDto = dbUserCreator.generateCreateDto();
        UserFullDto createdUser = userService.create(createDto);
        ChangeUserPasswordDto passwordDto = dbUserCreator.generateChangePasswordDto(createdUser.getId(),
                                                                                     createDto.getPassword());
        String token = jwtHelper.createToken(createdUser.getEmail());
        //when
        mockMvc.perform(request(PUT, "/users/password")
               .header("Authorization", token)
               .header("Content-Type", "application/json")
               .content(objectMapper.writeValueAsString(passwordDto)))
               .andExpect(status().isOk());
        UserFullDto foundUser = userService.findById(createdUser.getId());
        UserEntity foundEntity = userRepository.getById(foundUser.getId());
        //then
        Assertions.assertNotNull(foundUser.getId());
        Assertions.assertEquals(createdUser.getLastname(), foundUser.getLastname());
        Assertions.assertEquals(createdUser.getPhone(), foundUser.getPhone());
        if(passwordEncoder.matches(passwordDto.getNewPassword()+createDto.getEmail(), foundEntity.getPassword())){
            Assertions.assertTrue(1==1);
        }else {
            Assertions.assertTrue(1==2);
        }
    }

    @Test
    @Transactional
    public void changePassword_whenAnotherUser()throws Exception{
        //given
        UserCreateDto createDto = dbUserCreator.generateCreateDto();
        UserFullDto createdUser = userService.create(createDto);
        ChangeUserPasswordDto passwordDto = dbUserCreator.generateChangePasswordDto(createdUser.getId(),
                                                                                    createDto.getPassword());

        UserCreateDto createAnotherUser = dbUserCreator.generateCreateDto();
        UserFullDto createdAnotherUser = userService.create(createAnotherUser);
        String token = jwtHelper.createToken(createdAnotherUser.getEmail());

        //when
        mockMvc.perform(request(PUT, "/users/password")
               .header("Authorization", token)
               .header("Content-Type", "application/json")
               .content(objectMapper.writeValueAsString(passwordDto)))
               .andExpect(status().isMethodNotAllowed());
    }

    @Test
    public void changePassword_whenNotAuthenticated()throws Exception{
        //given
        UserCreateDto createDto = dbUserCreator.generateCreateDto();
        UserFullDto createdUser = userService.create(createDto);
        ChangeUserPasswordDto passwordDto = dbUserCreator.generateChangePasswordDto(createdUser.getId(),
                                                                                    createDto.getPassword());
        //when
        mockMvc.perform(request(PUT, "/users/password")
               .header("Content-Type", "application/json")
               .content(objectMapper.writeValueAsString(passwordDto)))
               .andExpect(status().isForbidden());
    }

    @Test
    @Transactional
    public void delete_happyPath_admin()throws Exception{
        //given
        UserCreateDto createDto = dbUserCreator.generateCreateDto();
        UserFullDto createdUser = userService.create(createDto);

        UserFullDto admin = dbUserCreator.addAdminToDb();
        String token = jwtHelper.createToken(admin.getEmail());
        //when
        mockMvc.perform(request(DELETE, "/users/"+createdUser.getId())
               .header("Authorization", token)
               .header("Content-Type", "application/json"))
               .andExpect(status().isOk());
        //then
        Assertions.assertNotNull(userRepository.getById(createdUser.getId()).getDeletedAt());
    }

    @Test
    @Transactional
    public void delete_happyPath_user()throws Exception{
        //given
        UserCreateDto createDto = dbUserCreator.generateCreateDto();
        UserFullDto createdUser = userService.create(createDto);

        String token = jwtHelper.createToken(createdUser.getEmail());
        //when
        mockMvc.perform(request(DELETE, "/users/"+createdUser.getId())
               .header("Authorization", token)
               .header("Content-Type", "application/json"))
               .andExpect(status().isOk());
        //then
        Assertions.assertNotNull(userRepository.getById(createdUser.getId()).getDeletedAt());
    }

    @Test
    @Transactional
    public void delete_whenAnotherUser()throws Exception{
        //given
        UserCreateDto createDto = dbUserCreator.generateCreateDto();
        UserFullDto createdUser = userService.create(createDto);

        UserFullDto anotherUser = userService.create(dbUserCreator.generateCreateDto());
        String token = jwtHelper.createToken(anotherUser.getEmail());
        //when
        mockMvc.perform(request(DELETE, "/users/"+createdUser.getId())
               .header("Authorization", token)
               .header("Content-Type", "application/json"))
               .andExpect(status().isMethodNotAllowed());
    }

    @Test
    @Transactional
    public void delete_whenNotAuthenticated()throws Exception{
        //given
        UserCreateDto createDto = dbUserCreator.generateCreateDto();
        UserFullDto createdUser = userService.create(createDto);

        //when
        mockMvc.perform(request(DELETE, "/users/"+createdUser.getId())
               .header("Content-Type", "application/json"))
               .andExpect(status().isForbidden());
    }

    @Test
    @Transactional
    public void boostBalance_happyPath_admin()throws Exception{
        //given
        UserCreateDto createDto = dbUserCreator.generateCreateDto();
        UserFullDto createdUser = userService.create(createDto);
        BoostBalanceDto balanceDto = dbUserCreator.generateBalanceDto(createdUser.getId(), createdUser.getEmail(),
                                                                      createDto.getPassword());
        UserFullDto admin = dbUserCreator.addAdminToDb();
        String token = jwtHelper.createToken(admin.getEmail());
        //when
        mockMvc.perform(request(PUT, "/users/boost")
               .header("Authorization", token)
               .header("Content-Type", "application/json")
               .content(objectMapper.writeValueAsString(balanceDto)))
               .andExpect(status().isOk());
        UserFullDto foundUser = userService.findById(createdUser.getId());
        //then
        Assertions.assertNotNull(foundUser.getId());
        Assertions.assertEquals(1000, foundUser.getBalance());
    }

    @Test
    @Transactional
    public void boostBalance_happyPath_user()throws Exception{
        //given
        UserCreateDto createDto = dbUserCreator.generateCreateDto();
        UserFullDto createdUser = userService.create(createDto);
        BoostBalanceDto balanceDto = dbUserCreator.generateBalanceDto(createdUser.getId(), createdUser.getEmail(),
                                                                      createDto.getPassword());
        String token = jwtHelper.createToken(createdUser.getEmail());
        //when
        mockMvc.perform(request(PUT, "/users/boost")
               .header("Authorization", token)
               .header("Content-Type", "application/json")
               .content(objectMapper.writeValueAsString(balanceDto)))
               .andExpect(status().isOk());
        UserFullDto foundUser = userService.findById(createdUser.getId());
        //then
        Assertions.assertNotNull(foundUser.getId());
        Assertions.assertEquals(1000, foundUser.getBalance());
    }

    @Test
    @Transactional
    public void boostBalance_whenAnotherUser()throws Exception{
        //given
        UserCreateDto createDto = dbUserCreator.generateCreateDto();
        UserFullDto createdUser = userService.create(createDto);
        BoostBalanceDto balanceDto = dbUserCreator.generateBalanceDto(createdUser.getId(), createdUser.getEmail(),
                                                                      createDto.getPassword());
        UserCreateDto anotherDto = dbUserCreator.generateCreateDto();
        UserFullDto anotherUser = userService.create(anotherDto);
        String token = jwtHelper.createToken(anotherUser.getEmail());
        //when
        mockMvc.perform(request(PUT, "/users/boost")
               .header("Authorization", token)
               .header("Content-Type", "application/json")
               .content(objectMapper.writeValueAsString(balanceDto)))
               .andExpect(status().isMethodNotAllowed());
    }

    @Test
    @Transactional
    public void boostBalance_whenNotAuthenticated()throws Exception{
        //given
        UserCreateDto createDto = dbUserCreator.generateCreateDto();
        UserFullDto createdUser = userService.create(createDto);
        BoostBalanceDto balanceDto = dbUserCreator.generateBalanceDto(createdUser.getId(), createdUser.getEmail(),
                                                                        createDto.getPassword());
        //when
        mockMvc.perform(request(PUT, "/users/boost")
               .header("Content-Type", "application/json")
               .content(objectMapper.writeValueAsString(balanceDto)))
               .andExpect(status().isForbidden());
    }

    @Test
    @Transactional
    public void changeRole_happyPath()throws Exception{
        //given
        UserCreateDto createDto = dbUserCreator.generateCreateDto();
        UserFullDto createdUser = userService.create(createDto);
        ChangeUserRoleDto  roleDto = dbUserCreator.generateRoleDto(createdUser.getId());

        UserFullDto admin = dbUserCreator.addAdminToDb();
        String token = jwtHelper.createToken(admin.getEmail());

        //when
        mockMvc.perform(request(PUT, "/users/role")
               .header("Authorization", token)
               .header("Content-Type", "application/json")
               .content(objectMapper.writeValueAsString(roleDto)))
               .andExpect(status().isOk());
        UserFullDto foundUser = userService.findById(createdUser.getId());

        //then
        Assertions.assertNotNull(foundUser.getId());
        Assertions.assertEquals(UserRole.ADMIN, foundUser.getRole());
    }

    @Test
    @Transactional
    public void changeRole_whenNotAdmin()throws Exception{
        //given
        UserCreateDto createDto = dbUserCreator.generateCreateDto();
        UserFullDto createdUser = userService.create(createDto);
        ChangeUserRoleDto  roleDto = dbUserCreator.generateRoleDto(createdUser.getId());

        String token = jwtHelper.createToken(createdUser.getEmail());
        //when
        mockMvc.perform(request(PUT, "/users/role")
               .header("Authorization", token)
               .header("Content-Type", "application/json")
               .content(objectMapper.writeValueAsString(roleDto)))
               .andExpect(status().isForbidden());
    }

    @Test
    @Transactional
    public void changeRole_whenNotAuthenticated()throws Exception{
        //given
        UserCreateDto createDto = dbUserCreator.generateCreateDto();
        UserFullDto createdUser = userService.create(createDto);
        ChangeUserRoleDto  roleDto = dbUserCreator.generateRoleDto(createdUser.getId());

        //when
        mockMvc.perform(request(PUT, "/users/role")
               .header("Content-Type", "application/json")
               .content(objectMapper.writeValueAsString(roleDto)))
               .andExpect(status().isForbidden());
    }

}
