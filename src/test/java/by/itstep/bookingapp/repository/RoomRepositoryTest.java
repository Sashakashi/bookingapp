package by.itstep.bookingapp.repository;
import by.itstep.bookingapp.BookingappApplicationTests;
import by.itstep.bookingapp.dto.hotel.HotelCreateDto;
import by.itstep.bookingapp.dto.hotel.HotelFullDto;
import by.itstep.bookingapp.entities.CountryEntity;
import by.itstep.bookingapp.entities.RoomEntity;
import by.itstep.bookingapp.service.HotelService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

public class RoomRepositoryTest extends BookingappApplicationTests {

    @Autowired
    private RoomRepository roomRepository;
    @Autowired
    private HotelService hotelService;
    @Autowired
    private CountryRepository countryRepository;
    @Autowired
    private HotelRepository hotelRepository;

    @Test
    @Transactional
    public void findByHotelAndGuests_happyPath(){
        //given
        HotelFullDto hotelDto = hotelService.create(generateCreateDto());
        //when
        List<RoomEntity> foundRoomsFor1 = roomRepository.findByHotelAndGuests(hotelDto.getId(), 1);
        //then
        Assertions.assertEquals(2, foundRoomsFor1.size());
    }

    private HotelCreateDto generateCreateDto(){
        HotelCreateDto hotelCreateDto = new HotelCreateDto();
        hotelCreateDto.setName(FAKER.name().firstName());
        hotelCreateDto.setAddress(FAKER.address().streetAddress());
        hotelCreateDto.setPrice(5.0);
        hotelCreateDto.setNumberOfRoomsFor1(2);
        hotelCreateDto.setNumberOfRoomsFor2(1);
        hotelCreateDto.setNumberOfRoomsFor3(2);
        hotelCreateDto.setNumberOfRoomsFor4(0);
        CountryEntity country = new CountryEntity();
        country.setName("test country"+Math.random());
        countryRepository.save(country);
        hotelCreateDto.setCountryId(country.getId());
        return hotelCreateDto;
    }
}
