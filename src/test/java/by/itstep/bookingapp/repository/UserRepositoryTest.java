package by.itstep.bookingapp.repository;
import by.itstep.bookingapp.BookingappApplicationTests;
import by.itstep.bookingapp.entities.UserEntity;
import by.itstep.bookingapp.entities.UserRole;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

public class UserRepositoryTest extends BookingappApplicationTests {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private BookingRepository bookingRepository;

    @Test
    @Transactional
    public void FindByEmail_happyPath(){
        //given
        UserEntity user = new UserEntity();
        user.setEmail(FAKER.internet().emailAddress());
        user.setName(FAKER.name().firstName());
        user.setLastname(FAKER.name().lastName());
        user.setPassword(FAKER.internet().password());
        user.setPhone(FAKER.phoneNumber().cellPhone());
        user.setRole(UserRole.USER);
        user.setBalance(0.0);
        user.setBlocked(false);
        userRepository.save(user);
        //when
        UserEntity foundUser = userRepository.findByEmail(user.getEmail()).orElseThrow();
        //then
        Assertions.assertNotNull(foundUser);
        Assertions.assertEquals(user.getPassword(), foundUser.getPassword());
        Assertions.assertEquals(user.getEmail(), foundUser.getEmail());
    }




}
